# Changelog

## Version 0.1 (November 19, 2015)
Initial release.

## Version 0.8 (April 6, 2015)
Version Alpha / Erste Abgabe EB

## Version 0.9 (April 6, 2015)
Version Beta / Überarbeitung Alpha nach Hinweisen Roger Klein

## Version 1 (April 6, 2015)
Version Final 1.0

