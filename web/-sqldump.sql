# ************************************************************
# Install red.eins contents
# Version 1.0
#
# https://gilles.ch
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'User name',
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'User email address',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'User password hash',
  `type` tinyint(1) NOT NULL COMMENT 'User access type (1=template-admin, 2=site-admin, 3=content-admin)',
  `startmodule` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'User startmodule (template, site, content)',
  `_create_time` datetime NOT NULL,
  `_update_time` datetime DEFAULT NULL,
  `_login_time` datetime DEFAULT NULL,
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`_id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `users` (`name`, `email`, `password`, `type`, `startmodule`, `_create_time`, `_update_time`, `_login_time`, `_id`)
VALUES
	('template-admin','','$2y$10$50EXbcIKxbUn1nmB6GFqO.SqEet5AEqEApnxvSP4/XLoTyivniLl6',1,'template','2016-01-02 10:10:10','2016-03-28 22:49:08','2016-04-13 21:17:15',1),
	('site-admin','','$2y$10$tYE.0h3GQJH6pgWLaiObl.R6nlQ60vjIYooR6S20Vi3C6KgulN/p.',2,'site','2016-01-02 10:10:10','2016-03-28 22:49:08','2016-04-05 22:58:39',2),
	('content-admin','','$2y$10$2EULUQm711VyCZNmoot8ze73sePvdF/IgSqHkBUqtahyrO3PWXjFW',0,'content','2016-01-02 10:10:10','2016-03-28 22:49:08','2016-04-13 20:43:48',3);
	
	
# Table languages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `languages`;

CREATE TABLE `languages` (
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'Language written out',
  `lang_code` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'Langauge code ISO 639-1',
  `_order` int(10) NOT NULL DEFAULT '0',
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`_id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `languages` (`name`, `lang_code`, `_order`, `_id`)
VALUES
	('Deutsch','de',0,1),
	('English','en',1,2);


# Table site_de
# ------------------------------------------------------------

DROP TABLE IF EXISTS `site_de`;

CREATE TABLE `site_de` (
  `home_site` int(10) NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci,
  `meta_keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`_id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `site_de` (`home_site`, `meta_title`, `meta_description`, `meta_keywords`, `_id`)
VALUES
	(1,'Globaler Titel deutsch','Globale Meta Beschreibung deutsch','schlüsselwörter,global,deutsch',1);


# Table site_en
# ------------------------------------------------------------

DROP TABLE IF EXISTS `site_en`;

CREATE TABLE `site_en` (
  `home_site` int(10) NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci,
  `meta_keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`_id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `site_en` (`home_site`, `meta_title`, `meta_description`, `meta_keywords`, `_id`)
VALUES
	(1,'Global title english','Global meta description english','keywords,global,english',1);


# Table sites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sites`;

CREATE TABLE `sites` (
  `template` int(10) unsigned NOT NULL,
  `redirect` int(10) NOT NULL DEFAULT '0',
  `_parent` int(10) NOT NULL,
  `_order` int(10) NOT NULL DEFAULT '0',
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`_id`,`template`),
  KEY `template` (`template`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


# Table sites_de
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sites_de`;

CREATE TABLE `sites_de` (
  `name_navigation` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name_url` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `meta_description` text COLLATE utf8_unicode_ci,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `visibility` tinyint(1) NOT NULL DEFAULT '1',
  `_create_time` datetime NOT NULL,
  `_update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`_id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


# Table sites_en
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sites_en`;

CREATE TABLE `sites_en` (
  `name_navigation` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name_url` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `meta_description` text COLLATE utf8_unicode_ci,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `visibility` tinyint(1) NOT NULL DEFAULT '1',
  `_create_time` datetime NOT NULL,
  `_update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`_id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


# Table template_modules
# ------------------------------------------------------------

DROP TABLE IF EXISTS `template_modules`;

CREATE TABLE `template_modules` (
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`_id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `template_modules` (`name`, `type`, `_id`)
VALUES
	('Varchar','VARCHAR(255) CHARACTER SET utf8',1),
	('Text','TEXT CHARACTER SET utf8',2),
	('Number','INT(11)',3),
	('Image','VARCHAR(63) CHARACTER SET utf8',4),
	('File','VARCHAR(63) CHARACTER SET utf8',5),
	('Date','DATE',6),
	('Boolean','TINYINT(1)',7),
	('Options','TEXT',8),
	('Password','VARCHAR(255) CHARACTER SET utf8',9),
	('Random','VARCHAR(255) CHARACTER SET utf8',10),
	('Site','INT(11)',11),
	('Spacer','CHAR(1) CHARACTER SET utf8',12),
	('Image manager','TEXT CHARACTER SET utf8',13),
	('File manager','TEXT CHARACTER SET utf8',14);


# Table templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `templates`;

CREATE TABLE `templates` (
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `file` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `max_entries` int(10) NOT NULL DEFAULT '1',
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`_id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;





/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
