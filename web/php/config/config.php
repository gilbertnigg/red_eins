<?php

  define('ENV', 'local');
//define('ENV', 'testing');
//define('ENV', 'production');

if (ENV == 'production') {
	//production
	error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
	ini_set('display_errors', 0);
} else {
	//local | testing
	error_reporting(-1);
	ini_set('display_errors', 1);
}

//set base directory of app
//-3 levels from this file
$s_app_basedir = dirname(
	dirname(
		dirname(__FILE__)
	)
).DIRECTORY_SEPARATOR;
define ('APP_BASEDIR', $s_app_basedir);


//db login
if (ENV == 'local') {
	$host      = 'localhost';
	$database  = 'red';
	$username  = 'root';
	$password  = 'admin';
	$base_url  = 'http://red/';
	$base_path = '';
} else {
	$host      = 'localhost';
	$database  = 'red';
	$username  = 'red_user';
	$password  = 'Admin_123';
	$base_url  = 'http://test.gilles.ch/';
	$base_path = 'red/';
}

//standard configurations
$a_conf['base_url']    = $base_url;
$a_conf['base_path']   = $base_path;
$a_conf['secret_key']  = 'bisschenPfeffer?';
$a_conf['modules']	   =  array('template' => array('name'		=>'Vorlagen',	
													'module'	=>'template', 
													'startpage'	=>'templates'),
													
								'site'	   => array('name'		=>'Seiten', 
													'module'	=>'site',
													'startpage'	=>'sites'),
													
								'content'  => array('name'		=>'Inhalte',
													'module'	=>'content',
													'startpage'	=>'sites'));