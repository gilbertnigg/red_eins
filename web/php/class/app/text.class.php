<?php
namespace app;

class text  {
	static function clearText($sText, $bHtml=true, $bSpecialChars=true, $bUpperLower=true) {
		$sText = trim($sText);
		if (!$bHtml) {
			$sText = htmlspecialchars($sText);
		}
		if (!$bSpecialChars) {
			$sText  = preg_replace('/[^a-zA-Z0-9_\-]+/', '', $sText);
		}
		if (!$bUpperLower) {
			$sText  = strtolower($sText);
		}
		return $sText;
	}
}