<?php
namespace app;

class template {
	protected $DB = false;
	public function __construct() {
		$this->DB = \app\db::load();
	}
	
	/**
	 * TEMPLATE HANDLING **********************************************************
	 */
	
	/**
	 * Create template
	 *
	 * @access	public
	 * @param	string	$sName			Template name
	 * @param	string	$sFile			Template file
	 * @param	integer $iMaxEntries	Template maximum items
	 * @uses	db
	 * @return	integer | boolean		New template id
	 */
	public function createTemplate($sName, $sFile, $iMaxEntries) {
		$sQuery = 'INSERT INTO `templates` (`name`, `file`, `max_entries`) 
				   VALUES ("'.$sName.'", "'.$sFile.'", '.$iMaxEntries.')';
		if (!$this->DB->query($sQuery))				 return false;
		$iInsertId = $this->DB->insert_id;
		if (!$this->createTemplateTable($iInsertId)) return false;
		return $iInsertId;
	}
	
	/**
	 * Update template
	 *
	 * @access	public
	 * @param	integer	$iTemplateId	Template ID
	 * @param	string	$sName			Template name
	 * @param	integer $iMaxEntries	Template maximum items
	 * @uses	db
	 * @return	boolean
	 */
	public function updateTemplate($iTemplateId, $sName, $sFile, $iMaxEntries) {
		$sQuery = 'UPDATE `templates` 
				   SET `name`="'.$sName.'", `file`="'.$sFile.'", `max_entries`='.$iMaxEntries.' 
				   WHERE _id='.$iTemplateId.' LIMIT 1';
		if ($this->DB->query($sQuery)) return true;
	}
	
	/**
	 * Delete template
	 *
	 * @access	public
	 * @param	integer	$iTemplateId	Template ID
	 * @uses	db
	 * @return	boolean
	 */
	public function deleteTemplate($iTemplateId) {
		$bDelete = $this->DB->deleteOne('templates', $iTemplateId);
		if (!$bDelete) return false;
		$bDrop = $this->DB->dropTable($this->templateTableName($iTemplateId));
		if (!$bDrop)   return false;
		return true;
	}
	
	/**
	 * Create DB-table for each template according to _id in DB-table "templates"
	 *
	 * @access	private
	 * @param	integer $iInsertId		Template ID from DB-table `templates`
	 * @param	array	$aModules		Used modules in current template
	 * @uses	db
	 * @return	boolean
	 */
	private function createTemplateTable ($iInsertId) {
		$sTableName = $this->templateTableName($iInsertId);
		$sQuery = '
			CREATE TABLE `'.$sTableName.'` (
				`name` char(50) CHARACTER SET utf8 NOT NULL DEFAULT "",
				`fieldname` char(50) CHARACTER SET utf8 NOT NULL DEFAULT "",
				`module` int(10) unsigned NOT NULL,
				`preview` tinyint(1) NOT NULL DEFAULT "0",
				`sort` tinyint(2) NOT NULL DEFAULT "0",
				`_order` int(10) NOT NULL DEFAULT "0",
				`_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`_id`,`module`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
		';
		if ($this->DB->query($sQuery)) return true;
	}
	
	/**
	 * Display all templates as array
	 *
	 * @access	public
	 * @uses	db
	 * @return	array
	 */
	public function listTemplates ($iTemplateId=false) {
		$sQuery		= 'SELECT name, file, max_entries, _id 
					   FROM `templates` '.($iTemplateId?'WHERE _id='.$iTemplateId:'').' 
					   ORDER BY name ASC';
		$aTemplates = $this->DB->getIdArray($sQuery);
		if ($iTemplateId) {
			 //not multidimensional for a single ID
			$aTemplates = $aTemplates[$iTemplateId];
		}
		return $aTemplates;
	}
	
	
	/**
	 * TEMPLATE MODULES HANDLING **************************************************
	 */

	/**
	 * Create template module
	 *
	 * @access	public
	 * @param	integer	$iTemplateId	Template ID
	 * @param	string	$sName			Module name
	 * @param	string	$sFieldName		Module fieldname for DB
	 * @param	integer $iModule		Module ID
	 * @param	boolean $bPreview		Module has preview
	 * @param	integer $iSort			Module sort order
	 *										-1 DESC
	 *										 0 no sorting
	 *										 1 ASC
	 * @uses	db
	 * @return	integer | boolean		New module id
	 */
	public function createTemplateModule($iTemplateId, $sName, $sFieldName, $iModule, $bPreview, $iSort) {
		$sTableName		= $this->templateTableName($iTemplateId);
		if (!$sFieldName) $sFieldName = $sName;
		$sFieldName		= \app\text::clearText($sFieldName, false, false, false);
		$aContentTables = $this->listAllTablesWithTemplate($iTemplateId);
		$sModuleType	= $this->DB->getOne('template_modules', $iModule, 'type');
		//insert module and set to first order
		$sQuery = 'INSERT INTO `'.$sTableName.'` 
				   (`name`, `fieldname`, `module`, `preview`, `sort`, `_order`) 
				   VALUES ("'.$sName.'", "'.$sFieldName.'", '.$iModule.', '.$bPreview.', '.$iSort.', -1)';
		if (!$this->DB->query($sQuery))		return false;
		$iInsertId = $this->DB->insert_id;
		//update associated content-tables
		foreach ($aContentTables as $sContentTableName) {
			$sQuery = 'ALTER TABLE `'.$sContentTableName.'` 
					   ADD `'.$sFieldName.'` '.$sModuleType.' FIRST';
			if (!$this->DB->query($sQuery)) return false;
		}
		//new module order
		$sQuery	= 'SELECT _id 
				   FROM `'.$sTableName.'` ORDER BY _order ASC';
		if (!$result = $this->DB->query($sQuery)) return false;
		$sModuleOrder = '';
		while ($row = $result->fetch_object()) {
			$sModuleOrder.= ($sModuleOrder?',':'').'module'.$row->_id;
		}
		if (!$this->reOrderTemplateModules($iTemplateId, $sModuleOrder)) return false;
		return $iInsertId;
	}
	
	/**
	 * Update template module
	 *
	 * @access	public
	 * @param	integer	$iTemplateId	Template ID
	 * @param	integer	$iModuleId		Module ID
	 * @param	string	$sName			Module name
	 * @param	string	$sFieldName		Module fieldname for DB
	 * @param	integer $iModule		Module item ID from DB-table `template_modules`
	 * @param	boolean $bPreview		Module has preview
	 * @param	integer $iSort			Module sort order
	 *										-1 DESC
	 *										 0 no sorting
	 *										 1 ASC
	 * @uses	db
	 * @return	boolean
	 */
	public function updateTemplateModule($iTemplateId, $iModuleId, $sName, $sFieldName, $iModule, $bPreview, $iSort) {
		$sTableName	   = $this->templateTableName($iTemplateId);
		if (!$sFieldName) $sFieldName = $sName;
		$sFieldName	   = \app\text::clearText($sFieldName, false, false, false);
		$sOldFieldName = $this->DB->getOne($sTableName, $iModuleId, 'fieldname');
		//update module
		$sQuery = 'UPDATE `'.$sTableName.'` 
				   SET `name`="'.$sName.'", `fieldname`="'.$sFieldName.'", 
					   `module`="'.$iModule.'", `preview`='.$bPreview.', `sort`='.$iSort.' 
				   WHERE _id='.$iModuleId.' LIMIT 1';
		if (!$this->DB->query($sQuery)) return false;
		//update associated content-tables
		$aContentTables = $this->listAllTablesWithTemplate($iTemplateId);
		$sModuleType	= $this->DB->getOne('template_modules', $iModule, 'type');
		#print_r($aContentTables);
		foreach ($aContentTables as $sTableName) {
			$sQuery = 'ALTER TABLE `'.$sTableName.'` CHANGE `'.$sOldFieldName.'` `'.$sFieldName.'` '.$sModuleType;
			//echo $sQuery;
			if (!$this->DB->query($sQuery)) return false;
		}
		return true;
	}
	
	/**
	 * Delete template module
	 *
	 * @access	public
	 * @param	integer	$iTemplateId	Template ID
	 * @param	integer	$iModuleId		Module ID
	 * @uses	db
	 * @return	boolean
	 */
	public function deleteTemplateModule($iTemplateId, $iModuleId) {
		$sTableName = $this->templateTableName($iTemplateId);
		$sFieldName = $this->DB->getOne($sTableName, $iModuleId, 'fieldname');
		$bDelete    = $this->DB->deleteOne($sTableName, $iModuleId);
		if (!$bDelete) return false;
		//update associated content-tables
		$aContentTables = $this->listAllTablesWithTemplate($iTemplateId);
		foreach ($aContentTables as $sTableName) {
			$sQuery = 'ALTER TABLE `'.$sTableName.'` DROP `'.$sFieldName.'`';
			if (!$this->DB->query($sQuery)) return false;
		}
		return true;
	}
	
	/**
	 * List all tables with the same template
	 *
	 * @access	public
	 * @param	integer	$iTemplateId	Template ID
	 * @uses	db
	 * @return	array					All table names with the same template
	 */
	public function listAllTablesWithTemplate($iTemplateId) {
		$sQuery		 = 'SELECT _id 
					    FROM `sites` 
					    WHERE `template` = '.$iTemplateId;
		$aTableIds   = $this->DB->getIdArray($sQuery);
		$aTableNames = array();
		foreach (\app\lang::$languages as $aValLang) {
			$sLangCode = $aValLang['langCode'];
			foreach ($aTableIds as $aVal) {
				$aTableNames[] = 'site_'.$aVal['_id'].'_'.$sLangCode;
			}
		}
		return $aTableNames;
	}
	
	/**
	 * Display all template modules of a single template as array
	 *
	 * @access	public
	 * @param	integer $iTemplateId	Template ID
	 * @uses	db
	 * @return	array					Templates-infos
	 */
	public function listTemplateModules ($iTemplateId) {
		$sTableName = $this->templateTableName($iTemplateId);
		$sQuery		= 'SELECT name, fieldname, module, preview, sort, _id 
					   FROM `'.$sTableName.'` ORDER BY _order ASC';
		$aTemplateModules = $this->DB->getIdArray($sQuery);
		return $aTemplateModules;
	}
	
	/**
	 * Get typw from moudle ID
	 *
	 * @access	public
	 * @param	integer $iModuleId
	 * @uses	db
	 * @return	array					Templates-infos
	 */
	public function templateModulesType ($iModuleId) {
		$sModuleType = $this->DB->getOne('template_modules', $iModuleId, 'type');
		return $sModuleType;
	}
	
	/**
	 * Display all template modules as array
	 *
	 * @access	public
	 * @uses	db
	 * @return	array					Templates-infos
	 */
	public function listAllTemplateModules () {
		$sQuery	= 'SELECT name, type, _id 
				   FROM `template_modules` ORDER BY name ASC';
		$aAllTemplateModules = $this->DB->getIdArray($sQuery);
		return $aAllTemplateModules;
	}
	
	/**
	 * Name of DB-table according to Template ID
	 *
	 * @access	private
	 * @param	integer $iTemplateId	_id from DB-table `templates`
	 * @uses	db
	 * @return	string Table name
	 */
	private function templateTableName ($iTemplateId) {
		return 'template_'.$iTemplateId;
	}
	
	
	/**
	 * ADMIN HELPERS **************************************************************
	 */
	 
	/**
	 * Get template files stored in template folder
	 *
	 * @access	private
	 * @return	array 	Filenames as array
	 */
	public function listTemplateFiles () {
		$aTemplateFiles = array();
		if ($oHandle = opendir(APP_BASEDIR.'/php/templates/')) {
		    while (false !== ($sFile = readdir($oHandle))) {
				if ($sFile[0]!='.') {
					$aTemplateFiles[] = $sFile;
				}
		    }
		}
		return $aTemplateFiles;
	}

	/**
	 * Select dropdown for templates
	 * @return	string
	 */
	public function listTemplatesToSelect ($sSelectName, $iSelectedTemplate, $sMode) {
		$aTemplates	= $this->listTemplates();
		if ($sMode=='update') {
			$sAlert = ' onchange="alert(\'Hinweis:\rDurch das Ändern der Vorlage werden die dazuügehörigen Inhalte gelöscht\')';
		} else {
			$sAlert = '';
		}
		$sSelect	= 
'<select name="'.$sSelectName.'" id="'.$sSelectName.'"'.$sAlert.'">';
		foreach ($aTemplates as $iTemplateId => $aTemplate) {
			if ($this->templateHasModule ($iTemplateId)) {
				$sTemplate = $aTemplate['name'];
				$sSelect.= '<option value="'.$iTemplateId.'"'.($iTemplateId==$iSelectedTemplate?' selected':'').'>'.
							$sTemplate.
							'</option>';
				}
		}
		$sSelect.= '</select>'.PHP_EOL;
		// TODO3: Template preview next to select
		/* TODO3: only if user type==1
		if (!empty($iSelectedTemplate)) {
			$sSelect.= '<p class="help-text nm">'.
					   '<a href="'.\admin\cms::adminHref('template', 'template').'&amp;template='.$iSelectedTemplate.'">'.
					   'Vorlage bearbeiten</a>'.
					   '</p>';
		}
		*/
		return $sSelect;
	}
	
	/**
	 * Helper for $this->listTemplatesToSelect()
	 *
	 * @access	private
	 * @uses	db
	 * @return	boolean
	 */
	private function templateHasModule ($iTemplateId) {
		$sTableName = $this->templateTableName ($iTemplateId);
		$sQuery = 'SELECT _id FROM `'.$sTableName.'` LIMIT 1';
		if (!$result = $this->DB->query($sQuery)) return false;
		if ($result->num_rows) return true;
		return false;
	}
	/**
	 * Functions for select tag on site view
	 *
	 * @access	public
	 * @uses	db
	 * @return	string
	 */
	public function listTemplateFilesToSelect ($sFieldName, $sSelected) {
		$aTemplates = $this->listTemplateFiles();
		$sSelect = '<select name="'.$sFieldName.'" id="'.$sFieldName.'">';
		$sSelect.= '<option value="0">Keine Template-Datei</option><option value="0">&nbsp;</option>';
		foreach ($aTemplates as $sTemplateFile) {
			$sSelect.= '<option value="'.$sTemplateFile.'"'.($sTemplateFile==$sSelected?' selected':'').'>'.$sTemplateFile.'</option>';
		}
		$sSelect.= '</select>';
		return $sSelect;
	}
	public function listTemplateModulesToSelect ($sFieldName, $iSelected) {
		$aModules = $this->listAllTemplateModules();
		$sSelect = '<select name="module" id="module">';
		foreach ($aModules as $key => $val) {
			$sSelect.= '<option value="'.$key.'"'.($key==$iSelected?' selected':'').'>'.$val['name'].' ('.$val['type'].')</option>';	
		}
		$sSelect.= '</select>';
		return $sSelect;
	}
	public function listTemplatePreviewToSelect ($sFieldName, $bSelected) {
		$aPreivews = array(1=>'sichtbar', 0=>'unsichtbar');
		$sSelect = '<select name="preview" id="preview">';
		foreach ($aPreivews as $key => $val) {
			$sSelect.= '<option value="'.$key.'"'.($key==$bSelected?' selected':'').'>'.$val.' </option>';	
		}
		$sSelect.= '</select>';	
		return $sSelect;
	}
	public function listTemplateSortToSelect ($sFieldName, $iSelected) {
		$aSorts = array(0=>'Keine Sortierung', 1=>'Aufsteigend', -1=>'Absteigend');
		$sSelect = '<select name="sort" id="sort">';
		foreach ($aSorts as $key => $val) {
			$sSelect.= '<option value="'.$key.'"'.($key==$iSelected?' selected':'').'>'.$val.' </option>';	
		}
		$sSelect.= '</select>';	
		return $sSelect;
	}
	
	/**
	 * Functions for icon class
	 *
	 * @access	public
	 * @uses	db
	 * @return	string
	 */
	 public function previewIcon ($iValue) {
	 	switch ($iValue) {
			case 1:
				return '<i class="fa fa-check-square-o"></i>';
			default:
				return '<i class="fa fa-square-o"></i>';
		}
	}
	 public function sortIcon ($iValue) {
	 	switch ($iValue) {
			case 1:
				return '<i class="fa fa-sort-amount-asc"></i>';
			case -1:
				return '<i class="fa fa-sort-amount-desc"></i>';
			default:
				return '<i class="fa fa-square-o"></i>';
		}
	}
	
	public function templateIsUsedBySite ($iTemplateId) {
		$sQuery = 'SELECT _id FROM `sites` WHERE template='.$iTemplateId.' LIMIT 1';
		if (!$result = $this->DB->query($sQuery)) return false;
		if ($result->num_rows) return true;
		return false;
	}
	
	
	/**
	 * TABLE DND HELPERS **********************************************************
	 */
	 
	/**
	 * Set new template modules order
	 *
	 * @access	public
	 * @param	integer $iTemplateId
	 * @param	string	$sModuleOrder	New order comma separated (like: 'module4,module14,module8...etc)
	 * @uses	db
	 * @return	sql query / boolean
	 */
	public function reOrderTemplateModules($iTemplateId, $sModuleOrder) {
		$aModuleOrder = explode(',', $sModuleOrder);
		$sTableName = $this->templateTableName($iTemplateId);
		$iNewOrder = 1;
		foreach ($aModuleOrder as $val) {
			$iModule = str_replace('module', '', $val);
			$sQuery = 'UPDATE `'.$sTableName.'` SET `_order`='.$iNewOrder.' 
					   WHERE _id = '.$iModule.' LIMIT 1';
			if (!$this->DB->query($sQuery)) return false;
			$iNewOrder++;
		}
		return true;
	}

	
	
}
