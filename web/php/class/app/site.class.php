<?php
namespace app;

class site {
	protected $DB = false;
	public function __construct() {
		$this->DB  = \app\db::load();
	}
	
	/**
	 * SITE HANDLING **************************************************************
	 */
	 
	/**
	 * Create site
	 *
	 * @access	public
	 * @param	integer	$iTemplate			Template-ID
	 * @param	integer	$iRedirect			Navigation redirect
	 * @param	integer $iParent			Navigation parent-ID
	 * @param	integer	$iOrder				Navigation order 
	 * @param	array	$aNameNavigation	Site name			Language separated (key=langCode => val=value)
	 * @param	array	$aNameUrl			Site url			Language separated (key=langCode => val=value)
	 * @param	array	$aMetaTitle			Site title			Language separated (key=langCode => val=value)
	 * @param	array	$aMetaDescription	Site description	Language separated (key=langCode => val=value)
	 * @param	array	$aMetaKeywords		Site keywords		Language separated (key=langCode => val=value)
	 * @param	array	$aVisibility		Site visibility		Language separated (key=langCode => val=value)
	 * @uses	db
	 * @return	integer						New site id
	 */
	public function createSite($iTemplate, $iRedirect, $iParent, $iOrder, 
							   $aNameNavigation, $aNameUrl, $aMetaTitle, 
							   $aMetaDescription, $aMetaKeywords, $aVisibility) {
		//insert
		$sQuery = 'INSERT INTO `sites` 
				   (`template`, `redirect`, `_parent`, `_order`) 
				   VALUES ('.$iTemplate.', '.$iRedirect.', '.$iParent.', '.$iOrder.')';
		if (!$this->DB->query($sQuery)) return false;
		$iInsertId = $this->DB->insert_id;
		foreach (\app\lang::$languages as $key => $val) {
			$sLangCode		 = $val['langCode'];
			$sNameNavigation = $aNameNavigation[$sLangCode];
			$sNameUrl		 = $aNameUrl[$sLangCode];
			//if nameUrl not set, take nameNavigation
			if (empty($sNameUrl) || is_numeric($sNameUrl)) {
				$sNameUrl = $sNameNavigation;
			}
			$sNameNavigation = \app\text::clearText($sNameNavigation, false, true, true);
			$sNameUrl		 = \app\text::clearText($sNameUrl, false, false, false);
			$sQuery = 'INSERT INTO `sites_'.$sLangCode.'` 
					   (`name_navigation`, `name_url`, `meta_title`, `meta_description`, 
					   `meta_keywords`, `visibility`, `_create_time`, `_id`) 
					   VALUES 
					   ("'.$sNameNavigation.'", "'.$sNameUrl.'", 
					   "'.$aMetaTitle[$sLangCode].'", "'.$aMetaDescription[$sLangCode].'", 
					   "'.$aMetaKeywords[$sLangCode].'", '.$aVisibility[$sLangCode].', 
					   "'.date('Y-m-d H:i:s').'", '.$iInsertId.')';
			if (!$this->DB->query($sQuery)) return false;
			//create site table
			$sQuery = $this->createSiteTable($iInsertId, $sLangCode, $iTemplate);
			if (!$sQuery) return false;
		}
		//reorder
		if (!$this->reOrderSite($iInsertId, $iParent, $iOrder)) return false;
		return $iInsertId;
	}
	
	/**
	 * Update site
	 *
	 * @access	public
	 * @param	integer	$iSiteId			Site ID
	 * @param	integer	$iTemplate			Template-ID
	 * @param	integer	$iRedirect			Navigation redirect
	 * @param	integer $iParent			Navigation parent-ID
	 * @param	integer	$iOrder				Navigation order 
	 * @param	array	$aNameNavigation	Site name			Language separated (key=langCode => val=value)
	 * @param	array	$aNameUrl			Site url			Language separated (key=langCode => val=value)
	 * @param	array	$aMetaTitle			Site title			Language separated (key=langCode => val=value)
	 * @param	array	$aMetaDescription	Site description	Language separated (key=langCode => val=value)
	 * @param	array	$aMetaKeywords		Site keywords		Language separated (key=langCode => val=value)
	 * @param	array	$aVisibility		Site visibility		Language separated (key=langCode => val=value)
	 * @uses	db
	 * @return	boolean
	 */
	public function updateSite($iSiteId, $iTemplateId, $iRedirect, $iParent, $iPosition, 
							   $aNameNavigation, $aNameUrl, $aMetaTitle, 
							   $aMetaDescription, $aMetaKeywords, $aVisibility) {
		//get current template
		$iCurrentTemplateId = $this->DB->getOne('sites', $iSiteId, 'template');
		//update sites table
		$sQuery =	  'UPDATE `sites` 
					   SET `template`="'.$iTemplateId.'", `redirect`='.$iRedirect.', 
					   	   `_parent`='.$iParent.', `_order`='.$iPosition.' 
					   WHERE _id = '.$iSiteId.' LIMIT 1';
		if (!$this->DB->query($sQuery)) return false;
		foreach (\app\lang::$languages as $key => $val) {
			$sLangCode		 = $val['langCode'];
			$sNameNavigation = $aNameNavigation[$sLangCode];
			$sNameUrl		 = $aNameUrl[$sLangCode];
			//if nameUrl not set, take nameNavigation
			if (empty($sNameUrl) || is_numeric($sNameUrl)) {
				$sNameUrl = $sNameNavigation;
			}
			$sNameNavigation = \app\text::clearText($sNameNavigation, false, true, true);
			$sNameUrl		 = \app\text::clearText($sNameUrl, false, false, false);
			$sQuery = 'UPDATE `sites_'.$sLangCode.'` 
					   SET `name_navigation`="'.$sNameNavigation.'", 
					   `name_url`="'.$sNameUrl.'", 
					   `meta_title`="'.$aMetaTitle[$sLangCode].'", 
					   `meta_description`="'.$aMetaDescription[$sLangCode].'", 
					   `meta_keywords`="'.$aMetaKeywords[$sLangCode].'", 
					   `visibility`='.$aVisibility[$sLangCode].'
					   WHERE _id = '.$iSiteId.' LIMIT 1';
			if (!$this->DB->query($sQuery)) return false;
		}
		//if different template delete and create content-tables
		if ($iCurrentTemplateId != $iTemplateId) {
			foreach (\app\lang::$languages as $val) {
				$sLangCode  = $val['langCode'];
				$sTableName = $this->siteTableName ($iSiteId, $sLangCode);
				//delete content table
				$this->DB->dropTable($sTableName);
				//create content table
				$sQuery = $this->createSiteTable($iSiteId, $sLangCode, $iTemplateId);
				if (!$sQuery) return false;
			}
		}
		return true;
	}
	
	/**
	 * Delete site and its sub sites
	 *
	 * @access	public
	 * @param	integer	$iSiteId	Site ID
	 * @uses	db
	 * @return	boolean
	 */
	 public function deleteSiteAndSubSites($iSiteId) {
		//get sub sites
		$aMultiSubSites = $this->tree ($iSiteId);
		$aSubSites		= $this->getMultiarrayKeys($aMultiSubSites);
		//add current site to array
		$aSubSites[]	= $iSiteId;
		foreach ($aSubSites as $iSiteId) {
			if (!$this->deleteSite($iSiteId)) return false;
		}
		return true;
	}
	/**
	 * Helper for deleteSiteAndSubSites gets all multilevel key to flat array
	 * @return array
	 */
	private function getMultiarrayKeys($ar) { 
		foreach($ar as $k => $v) { 
			$keys[] = $k; 
			if (is_array($ar[$k])) {
				$keys = array_merge($keys, $this->getMultiarrayKeys($ar[$k]));
			}
		} 
		return $keys; 
	} 
	
	/**
	 * Delete single site
	 *
	 * @access	public
	 * @param	integer	$iSiteId	Site ID
	 * @uses	db
	 * @return	boolean
	 */
	public function deleteSite($iSiteId) {
		$bDelete = $this->DB->deleteOne('sites', $iSiteId);
		if (!$bDelete) return false;
		foreach (\app\lang::$languages as $key => $val) {
			$sLangCode = $val['langCode'];
			$bDelete   = $this->DB->deleteOne('sites_'.$sLangCode, $iSiteId);
			$bDrop	   = $this->DB->dropTable($this->siteTableName($iSiteId, $sLangCode));
			if (!$bDrop || !$bDelete) return false;
		}
		return true;
	}
	
	/**
	 * SITE HELPERS **************************************************************
	 */

	/**
	 * Create DB-table for each site according to _id in DB-table `sites`
	 *
	 * @access	private
	 * @param	integer $iSiteId		site-ID from DB-table `sites`
	 * @param	string	$sLangCode		Lang-Code
	 * @param	integer	$iTemplateId	Template-ID
	 * @uses	db
	 * @return	boolean
	 */
	private function createSiteTable ($iSiteId, $sLangCode, $iTemplateId) {
		//foreach (lang::$languages as $key => $val) {
		//$sLangCode = $val['langCode'];
		$sTableName   = $this->siteTableName($iSiteId, $sLangCode);
		$aSiteModules = $this->siteModules ($iTemplateId);
		$sSiteModules = implode(',', $aSiteModules);
		$sQuery = 'CREATE TABLE `'.$sTableName.'` (
				'.$sSiteModules.',
				`_order` int(10) NOT NULL DEFAULT "0",
				`_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;';
		if ($this->DB->query($sQuery)) return true;
	}
	
	
	/**
	 * Site modules according to associated template
	 *
	 * @access	private
	 * @param	integer $iTemplateId	template-ID
	 * @uses	db
	 * @return	array
	 */
	private function siteModules ($iTemplateId) {
		$sTemplateTable = 'template_'.$iTemplateId;
		$sQuery = 'SELECT * FROM `'.$sTemplateTable.'` ORDER BY _order ASC';
		if (!$result = $this->DB->query($sQuery)) return false;
		while ($row = $result->fetch_object()) {
			$sQueryModule = 'SELECT type FROM `template_modules` WHERE _id = '.$row->module.' LIMIT 1';
			if (!$resultModule = $this->DB->query($sQueryModule)) return false;
			$rowModule    = $resultModule->fetch_object();
			$sFieldName   = $row->fieldname;
			$sFieldType   = $rowModule->type;
			$aModules[]   = '`'.$sFieldName.'` '.$rowModule->type;
		}
		return $aModules;
	}
	
	/**
	 * Site all infos
	 *
	 * @access	public
	 * @param	string	$iSiteId	Site-ID
	 * @param	string	$sLangCode	Language-code
	 * @uses	db
	 * @return	multidimensional array
	 */
	public function siteInfo ($iSiteId=NAVID, $sLangCode=LANG) {
		$aContents = $_aContents = $_aContentsLang = array();
		//empty values for create
		if (!$iSiteId) {
			//general info
			$sQuery = 'SELECT * FROM `sites` LIMIT 1';
			if (!$result = $this->DB->query($sQuery)) return false;
			while ($aFieldInfo = $result->fetch_field()) {
				$_aContents[$aFieldInfo->name] = '';
	    	}
			//lang info
			$sQuery = 'SELECT * FROM `sites_'.$sLangCode.'` LIMIT 1';
			if (!$resultLang = $this->DB->query($sQuery)) return false;
			while ($aFieldInfo = $resultLang->fetch_field()) {
				$_aContentsLang[$aFieldInfo->name] = '';
	    	}
			$_aContentsLang['visibility'] = 1;
			$aContents = array_merge($_aContents, $_aContentsLang);
		//values for reading
		} else {
			//general info
			$sQuery = 'SELECT * FROM sites WHERE _id='.$iSiteId.' LIMIT 1';
			if (!$_aContents = $this->DB->getIdArray($sQuery)[$iSiteId]) return false;
			//lang info
			$sQuery = 'SELECT * FROM sites_'.$sLangCode.' WHERE _id='.$iSiteId.' LIMIT 1';
			if (!$_aContentsLang = $this->DB->getIdArray($sQuery)[$iSiteId]) return false;
			$aContents = array_merge($_aContents, $_aContentsLang);
		}
		return $aContents;
	}
	
	
	/**
	 * HTML HELPERS ****************************************************************
	 */
	 
	/**
	 * Site tree
	 *
	 * @access	public
	 * @param	integer $iParent	Root site-ID
	 * @uses	db
	 * @return	multidimensional array
	 */
	public function tree ($iParent=0) {
		$aNavRef	 = array();
		$aNavRefRefs = array();
		$result = $this->DB->query('SELECT _parent, _id FROM sites ORDER BY _order ASC');
		while ($row = $result->fetch_object()) {
			$aThisref = &$aNavRefRefs[$row->_id];
			if ($row->_parent == $iParent) {
				$aNavRef[$row->_id] = &$aThisref;
			} else {
				$aNavRefRefs[$row->_parent][$row->_id] = &$aThisref;
			}
		}
		return $aNavRef;
	}
	
	/**
	 * Site tree
	 *
	 * @access	public
	 * @param	array	$aSites from '->tree' function
	 * @param	string	$sLangCode
	 * @uses	db
	 * @return	string
	 */
	public function treeNavigation($aSites, $sLangCode=LANG) {
		$sReturn = '';
		if (is_array($aSites)){
			foreach($aSites as $iSiteId => $oTree) {
				$sNameNavigation = $this->DB->getOne('sites_'.$sLangCode, $iSiteId, 'name_navigation');
				//TODO3:
				//better oop for inline namespaces
				$sSpan = 
'<span title="Seite verschieben" class="_bt drag-handle">'.$sNameNavigation.'</span>'.
'<span onClick="update_site(\''.\admin\cms::adminHref('site').'&navid='.$iSiteId.'\')">'.
'<i class="fa fa-pencil-square-o _bt" title="Seite bearbeiten"></i></span> '.
'<span onClick="create_site(\''.\admin\cms::adminHref('site').'&create&parent='.$iSiteId.'\')">'.
'<i class="fa fa-plus _bt" title="Seite hinzufügen"></i></span> '.
'<span onClick="delete_site(\''.\admin\cms::adminHref('sites').'&delete_site='.$iSiteId.'\')">'.
'<i class="fa fa-times _bt" title="Seite löschen"></i></span>';
				$sReturn = $sReturn.
'<li id="item'.$iSiteId.'" data-langcode="'.LANG.'">'.$sSpan.$this->treeNavigation($oTree, $sLangCode).'</li>';
			}
		}
		if ($sReturn) {
			$sReturn = '<ul>'.$sReturn.'</ul>'.PHP_EOL;
		}
		return $sReturn; 
	}
	
	/**
	 * ADMIN HELPERS **************************************************************
	 */
	 	
	/**
	 * Select dropdown for parent sites
	 * @return	string
	 */
	public function listParentsToSelect($sSelectName, $sSelectedSite, $aSites, $sLangCode=LANG) {
		$sSelect = '<select name="'.$sSelectName.'" id="'.$sSelectName.'">';
		$sSelect.= '<option value="0">Root</option>';
		if (is_array($aSites)){
			foreach($aSites as $iSiteId => $oTree) {
				$sNameNavigation = $this->DB->getOne('sites_'.$sLangCode, $iSiteId, 'name_navigation');
				$sSelect.= '<option value="'.$iSiteId.'"'.($iSiteId==$sSelectedSite?' selected':'').'>'.
							$sNameNavigation.
							'</option>'.
							$this->treeNavigation($oTree, $sLangCode);
			}
		}
		$sSelect.= '</select>'.PHP_EOL;
		return $sSelect; 
	}
	/**
	 * Select dropdown for redirect site
	 * @return	string
	 */
	public function listRedirectsToSelect($sSelectName, $sSelectedSite, $aSites, $sLangCode=LANG) {
		$sSelect = '<select name="'.$sSelectName.'" id="'.$sSelectName.'">';
		$sSelect.= '<option value="0">Keine Weiterleitung</option>';
		if (is_array($aSites)){
			foreach($aSites as $iSiteId => $oTree) {
				$sNameNavigation = $this->DB->getOne('sites_'.$sLangCode, $iSiteId, 'name_navigation');
				$sSelect.= 
'<option value="'.$iSiteId.'"'.($iSiteId==$sSelectedSite?' selected':'').'>'.
$sNameNavigation.
'</option>'.$this->treeNavigation($oTree, $sLangCode);
			}
		}
		$sSelect.= '</select>'.PHP_EOL;
		return $sSelect; 
	}
	/**
	 * Select dropdown for show or hide site
	 * @return	string
	 */
	public function listVisibilityToSelect ($sSelectName, $bVisibility) {
		$aVisibility = array(1=>'Sichtbar', 0=>'Unsichtbar');
		$sSelect = '<select name="'.$sSelectName.'" id="'.$sSelectName.'">';
		foreach ($aVisibility as $key => $val) {
			$sSelect.= 
'<option value="'.$key.'"'.($key==$bVisibility?' selected':'').'>'.$val.' </option>';	
		}
		$sSelect.= '</select>'.PHP_EOL;
		return $sSelect;
	}
	
	/**
	 * Name of DB-table according to site-ID and lang-code
	 *
	 * @access	private
	 * @param	integer $iSiteId	_id from DB-table `site`
	 * @param	string	$sLangCode	Lang-code
	 * @uses	db
	 * @return	string
	 */
	private function siteTableName ($iSiteId, $sLangCode) {
		return 'site_'.$iSiteId.'_'.$sLangCode;
	}

	
	/**
	 * JSTREE HELPERS *************************************************************
	 */

	/**
	 * Set new site order
	 *
	 * @access	public
	 * @param	integer $iSiteId
	 * @param	integer	$iParent	Parent site ID
	 * @param	integer $iOrder		New order index
	 * @param	string	$sLangCode
	 * @uses	db
	 * @return	string
	 */
	public function reOrderSite($iSiteId, $iParent, $iOrder, $sLangCode=LANG) {
		//get old parent
		$iParentOld = $this->DB->getOne('sites', $iSiteId, '_parent');
		//select all orders
		$sQuery = 'SELECT _id FROM `sites` WHERE _parent='.$iParent.' 
				   ORDER BY _order ASC';
		if (!$result = $this->DB->query($sQuery)) return false;
		$aOrder = $this->DB->getIdArray($sQuery);
		//only reorder / no new parent
		if ($iParent == $iParentOld) {
			unset($aOrder[$iSiteId]);
		} else {
			$sQuery = 'UPDATE `sites` SET `_parent`='.$iParent.' 
					   WHERE _id = '.$iSiteId.' LIMIT 1';
			if (!$this->DB->query($sQuery)) return false;
		}
		//itemids to level 0 for splicing
		$aOrder = array_map(function($a) {
			return array_pop($a);
		}, $aOrder);
		array_splice($aOrder, $iOrder, 0, $iSiteId);
		//update
		$iNewOrder = 1;
		foreach ($aOrder as $val) {
			$sQuery = 'UPDATE `sites` SET `_order`='.$iNewOrder.' 
					   WHERE _id = '.$val.' LIMIT 1';
			if (!$this->DB->query($sQuery)) return false;
			$iNewOrder++;
		}
		return true;
	}
}
