<?php
namespace app;

class lang {
	protected $DB = false;
	public static $langCode;
	public static $languages;
	public function __construct() {
		$this->DB = \app\db::load();
		self::$languages = $this->getLangs();
		self::$langCode  = $this->getLangCode();
		define('LANG', $this->getLangCode());		
	}

	/**
	 * Get all site languages
	 *
	 * @access	private
	 * @uses	db
	 * @return	array
	 */
	private function getLangs() {
		$result = $this->DB->query('
			SELECT name, lang_code FROM `languages` ORDER BY _order ASC
		');
		$aLanguages = array();
		while ($row = $result->fetch_object()) {
			/*TODO: like this: 
			$aLanguages[$row->lang_code] = $row->name;*/
			$aLanguages[] = array('langCode'=>$row->lang_code, 
								  'langName'=>$row->name);
		}
		$result->free();
		return $aLanguages;
	}

	/**
	 * Get current languate-code (like: 'de', 'en'... etc)
	 *
	 * @access	private
	 * @return	array
	 */
	private function getLangCode() {
		$_sLangCode = false;
		if ($this->getSiteLangCode()) {
			$_sLangCode = $this->getSiteLangCode();
		} else if ($this->getBrowserLangCode()) {
			$_sLangCode = $this->getBrowserLangCode();
		}
		if ($this->checkLangCode($_sLangCode)) {
			return $_sLangCode;			
		} else {
			return $this->getStandardLangCode();
		}
	}

	/**
	 * Get url-lang-value
	 *
	 * @access	private
	 * @return	string
	 */
	private function getSiteLangCode() {
		if (isset($_GET['lang'])) {
			$_sLang = preg_replace('/[^a-zA-Z_\-]+/', '', $_GET['lang']);
			return $_sLang;			
		} else {
			return false;			
		}
	}

	/**
	 * Get browser-lang-value
	 *
	 * @access	private
	 * @return	string
	 */
	private function getBrowserLangCode() {
		if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
			$_sLangCode = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
			$_aLangCode = explode(',', $_sLangCode);
			$_aLangCode = explode('-', $_aLangCode[0]);
			return $_aLangCode[0];
		}
		return false;
	}

	/**
	 * Check if lang code is available
	 *
	 * @access	private
	 * @return	boolean
	 */
	private function checkLangCode($_sLangCode) {
		$aCheckLangs = array();
		foreach ($this->getLangs() as $aVal) {
			$aCheckLangs[] = $aVal['langCode'];
		}
		if (in_array($_sLangCode, $aCheckLangs)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Get first lang code (if all checks failed)
	 *
	 * @access	private
	 * @return	string
	 */
	private function getStandardLangCode() {
		return $this->getLangs()[0]['langCode'];
	}
}