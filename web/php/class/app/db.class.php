<?php
namespace app;

class dbConnect extends \mysqli {
	public function __construct($host, $username, $password, $database) {
		parent::__construct($host, $username, $password, $database);
	}

	/**
	 * QUERY HANDLING **********************************************************
	 */
	public function countRows($sTablename) {
		$result = $this->query('
			SELECT _id FROM `'.$sTablename.'`
		');
		if ($result) {
			return $result->num_rows;
		}
	}
	public function getRow($sTablename, $iItemid) {
		$result = $this->query('
			SELECT * FROM `'.$sTablename.'` WHERE _id='.$iItemid.' LIMIT 1
		');
		if ($result) {
			return $result->fetch_all(MYSQLI_ASSOC);
		}
	}
	public function getOne($sTablename, $iItemid, $sField) {
		$sQuery = 'SELECT '.$sField.' FROM `'.$sTablename.'` WHERE _id='.$iItemid.' LIMIT 1';
		if ($result = $this->query($sQuery)) {
			$row = $result->fetch_object();
			return $row->$sField;
		}
	}
	public function getIdArray($sQuery) {
		if (!$result = $this->query($sQuery)) return false;
		$_aResult = $result->fetch_all(MYSQLI_ASSOC);
		$aResult  = array();
		//bind array to _id
		foreach ($_aResult as $aVal) {
			if (isset($aVal['_id'])) {
				$aResult[$aVal['_id']] = $aVal;	
			}
		}
		return $aResult;
	}
	public function deleteOne($sTablename, $iItemid) {
		$sQuery = 'DELETE FROM `'.$sTablename.'` WHERE _id='.$iItemid.' LIMIT 1';
		if ($result = $this->query($sQuery)) {
			return true;
		}
	}
	public function dropTable($sTablename) {
		$sQuery = 'DROP TABLE `'.$sTablename.'`';
		if ($result = $this->query($sQuery)) {
			return true;
		}
	}
	//TODO: add normal query here
}

//db singleton
//TODO 3: DB klasse in elternklasse
 class db {
	static private $instance = null;
	static private $host = null;
	static private $username = null;
	static private $password = null;
	static private $database = null;
	static public function load() {
		if (null === self::$instance) {
			#self::$instance = new \mysqli($host...);
			self::$instance = new dbConnect(self::$host, self::$username, self::$password, self::$database);
			self::$instance->query('SET NAMES "utf8"');
		}
		return self::$instance;
	}
	public function __construct($host, $username, $password, $database){
		self::$host=$host; self::$username=$username; self::$password=$password; self::$database=$database;
	}
	public function __clone(){}
 }