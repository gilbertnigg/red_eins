<?php
namespace app;

class content {
	protected $DB = false;
	public function __construct() {
		$this->DB = \app\db::load();
	}
	

	/**
	 * CONTENT HANDLING ***********************************************************
	 */
	
	/**
	 * Create content
	 *
	 * @access	public
	 * @param	array	$aContents	Content Infos
	 * @param	integer	$iSiteId	Site ID
	 * @param	integer	$sLangCode	Language Code
	 * @uses	db
	 * @return	integer				New content ID
	 */
	public function createContent($aContents, $iSiteId=NAVID, $sLangCode=LANG) {
		$aInsertFields = $aInsertValues = array();
		foreach ($aContents as $sFieldName => $sFieldContent) {
			if ($sFieldName[0] != '-') {
				$aInsertFields[] = '`'.$sFieldName.'`';
				$aInsertValues[] = '"'.$sFieldContent.'"';
			}
		}
		$sQuery = 'INSERT INTO `'.$this->contentTableName ($iSiteId, $sLangCode).'` 
				   ('.implode(',', $aInsertFields).') 
				   VALUES ('.implode(',', $aInsertValues).')';
		if (!$this->DB->query($sQuery)) return false;
		$iInsertId = $this->DB->insert_id;
		return $iInsertId;
	}
	
	/**
	 * Update content
	 *
	 * @access	public
	 * @param	array	$aContents	Content Infos
	 * @param	integer	$iItemid	Item ID
	 * @param	integer	$iSiteId	Site ID
	 * @param	integer	$sLangCode	Language Code
	 * @uses	db
	 * @return	boolean
	 */
	public function updateContent($aContents, $iItemid=ITEMID, $iSiteId=NAVID, $sLangCode=LANG) {
		$aUpdateFields = array();
		foreach ($aContents as $sFieldName => $sFieldContent) {
			if ($sFieldName[0] != '-') {
				$aUpdateFields[] = '`'.$sFieldName.'`="'.$sFieldContent.'"';
			}
		}
		$sQuery = 'UPDATE `'.$this->contentTableName ($iSiteId, $sLangCode).'` 
				   SET '.implode(',', $aUpdateFields).' WHERE _id='.$iItemid.' LIMIT 1';
		if ($this->DB->query($sQuery)) return true;
	}
	
	/**
	 * Delete content
	 *
	 * @access	public
	 * @param	integer	$iSiteId	Site ID
	 * @param	integer	$iItemid	Item ID
	 * @param	integer	$sLangCode	Language Code
	 * @uses	db
	 * @return	boolean
	 */
	public function deleteContent($iSiteId=NAVID, $iItemid=ITEMID, $sLangCode=LANG) {
		$sTableName = $this->contentTableName ($iSiteId, $sLangCode);
		$bDelete	= $this->DB->deleteOne($sTableName, $iItemid);
		if ($bDelete) return true;
	}
	
	/**
	 * PREVIEW HANDLING ***********************************************************
	 */

	/**
	 * Get content preview fields
	 *
	 * @access	public
	 * @param	integer	$iSiteId	Site ID
	 * @param	integer	$sLangCode	Language Code
	 * @uses	db
	 * @return	array
	 */
	public function listContentPreviewFields ($iSiteId=NAVID, $sLangCode=LANG) {
		$aPreviewFields = array();
		$iTemplateId	= $this->DB->getOne('sites', $iSiteId, 'template');
		$sQuery = 'SELECT * FROM `template_'.$iTemplateId.'` WHERE preview=1 ORDER BY _order ASC';
		if (!$result = $this->DB->query($sQuery)) return false;				
		while ($row = $result->fetch_object()) {
			$aPreviewFields[$row->_id] = array('name'	   => $row->name, 
											   'fieldname' => $row->fieldname,
											   'module'	   => $row->module
										 );
		}
		return $aPreviewFields;
	}
	
	/**
	 * Get content preview
	 *
	 * @access	public
	 * @param	integer	$iSiteId	Site ID
	 * @param	integer	$sLangCode	Language Code
	 * @uses	db
	 * @return	array
	 */
	public function listContentPreview ($iSiteId, $sLangCode) {
		//order
		$aSorts = $this->sortContentFields ($iSiteId);
		if (count($aSorts)) {
			$sSorts = ' ORDER BY '.implode(', ', $aSorts);
		} else {
			$sSorts = '';
		}
		//get info
		$aPreviewFields =  $this->listContentPreviewFields ($iSiteId, $sLangCode);
		$sQuery			= 'SELECT * FROM `'.$this->contentTableName($iSiteId, $sLangCode).'`'.$sSorts;
		if (!$result = $this->DB->query($sQuery)) return false;				
		$aContents = array();
		while ($row = $result->fetch_object()) {
			foreach ($aPreviewFields as $aVal) {
				$sFieldName = $aVal['fieldname'];
				$aContents[$sFieldName] = $aVal;
			}
		}
		return $aContents;
	}
	
	/**
	 * Get all conteint item IDs
	 *
	 * @access	public
	 * @param	integer	$iSiteId	Site ID
	 * @param	integer	$iItemid	Item ID
	 * @param	integer	$sLangCode	Language Code
	 * @uses	db
	 * @return	array
	 */
	public function itemIds ($iSiteId=NAVID, $sLangCode=LANG) {
		//get order
		$aSorts = $this->sortContentFields ($iSiteId);
		if (count($aSorts)) {
			$sSorts = ' ORDER BY '.implode(', ', $aSorts);
		} else {
			$sSorts = '';
		}
		//get IDSs
		$aReturn = array();
		$sQuery	 = 'SELECT _id FROM `'.$this->contentTableName($iSiteId, $sLangCode).'`'.$sSorts;
		if (!$result = $this->DB->query($sQuery)) return false;
		while ($row=$result->fetch_object()) {
			$aReturn[] = $row->_id;
		}
		return $aReturn;
	}
	
	/**
	 * Get sort mode for sorting db fields (like: 'name ASC')
	 *
	 * @access	public
	 * @param	integer	$iSiteId	Site ID
	 * @uses	db
	 * @return	array
	 */
	public function sortContentFields ($iSiteId) {
		//get order
		$iTemplateId = $this->DB->getOne('sites', $iSiteId, 'template');
		$sQuery		 = 'SELECT sort, fieldname FROM `template_'.$iTemplateId.'` 
						WHERE sort!=0 ORDER BY _order ASC';
		if (!$result = $this->DB->query($sQuery)) return false;	
		$aSorts = array();		
		while ($row = $result->fetch_object()) {
			$sSort = ($row->sort == 1 ? 'ASC' : 'DESC');
			$aSorts[] = '`'.$row->fieldname.'` '.$sSort;
		}
		return $aSorts;
	}
	
	/**
	 * Get sort mode for sorting db fields (like: 'name ASC')
	 *
	 * @access	public
	 * @param	integer	$sFieldName	DB field name
	 * @param	integer	$iSiteId	Site ID
	 * @param	integer	$iItemid	Item ID
	 * @param	integer	$sLangCode	Language Code
	 * @uses	db
	 * @return	string
	 */
	public function fieldContent ($sFieldName, $iItemid=ITEMID, $iSiteId=NAVID, $sLangCode=LANG) {
		if (!$iItemid) return '';
		$sContent = $this->DB->getOne($this->contentTableName($iSiteId, $sLangCode), $iItemid, $sFieldName);
		return $sContent;
	}
	
	
	
	/**
	 * CONTENT HELPERS ************************************************************
	 */	
	/**
	 * List field info of content
	 *
	 * @access	public
	 * @param	integer	$iSiteId	Site ID
	 * @param	integer	$sLangCode	Language Code
	 * @uses	db
	 * @return	array
	 */
	public function listContentFields ($iSiteId=NAVID, $sLangCode=LANG) {
		$aContentFields = array();
		$iTemplateId	= $this->DB->getOne('sites', $iSiteId, 'template');
		$sQuery = 'SELECT * FROM `template_'.$iTemplateId.'` ORDER BY _order ASC';
		if (!$result = $this->DB->query($sQuery)) return false;				
		while ($row = $result->fetch_object()) {
			$aContentFields[$row->_id] = array('name'		=> $row->name, 
											   'fieldname'	=> $row->fieldname,
											   'module'		=> $row->module
										 );
		}
		return $aContentFields;
	}
	
	/**
	 * Name of DB-table according to site-ID
	 *
	 * @access	private
	 * @param	integer $iSiteId	_id from DB-table `sites`
	 * @uses	db
	 * @return	string
	 */
	private function contentTableName ($iSiteId, $sLangCode) {
		$sTableName = 'site_'.$iSiteId.'_'.$sLangCode;
		return $sTableName;
	}

	
	/**
	 * ADMIN HELPERS **************************************************************
	 */
	
	/**
	 * Get sites as list/tree
	 *
	 * @access	private
	 * @param	integer	$iSiteId	Site ID
	 * @param	integer	$sLangCode	Language Code
	 * @uses	db
	 * @return	string
	 */
	public function treeNavigation($aSites, $sLangCode=LANG) {
		$sReturn = '';
		if (is_array($aSites)){
			foreach($aSites as $iSiteId => $s_nav_val) {
				$sNameNavigaion = $this->DB->getOne('sites_'.$sLangCode, $iSiteId, 'name_navigation');
				$sAnchor = '<a href="'.\admin\cms::adminHref('contents').'&amp;navid='.$iSiteId.'">
						   '.$sNameNavigaion.
						   '</a>';
				$sSpan = 
'<span onClick="update_content(\''.\admin\cms::adminHref('contents').'&amp;navid='.$iSiteId.'\')">'.$sNameNavigaion.'</span> ';
				$sReturn.= '<li id="item'.$iSiteId.'">'.$sSpan.$this->treeNavigation($s_nav_val, $sLangCode).'</li>';
			}
		}
		if ($sReturn) {
			$sReturn = '<ul>'.$sReturn.'</ul>'.PHP_EOL;
		}
		return $sReturn; 
	}
}
