<?php
	
/**
 * Load the class specified 
 * @param	string		Class to load
 */
function autoload($sClassName) {
	/* Using the built-in autoloader as it's faster than a PHP implementation.
	 * Replace class separator with slashes to use a directory structure
	 */
	set_include_path(get_include_path().PATH_SEPARATOR.APP_BASEDIR.'php/class');
	set_include_path(get_include_path().PATH_SEPARATOR.APP_BASEDIR.'admin/php/class');
	spl_autoload_extensions('.class.php');
	//watch out: directories and files always strtolower
	$sFileName = str_replace('\\', DIRECTORY_SEPARATOR, $sClassName);
	return spl_autoload($sFileName);
}
spl_autoload_register('autoload');