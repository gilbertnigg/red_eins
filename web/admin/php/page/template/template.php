<?php
//load classes
$oCms		= new \admin\cms();
$oTemplate	= new \app\template();

//set variables
if (isset($_GET['template'])) {
	$i_template = $_GET['template'];
	$a_template				= $oTemplate->listTemplates($i_template);
	$s_template_name		= $a_template['name'];
	$s_template_file		= $a_template['file'];
	$i_template_max_entries	= $a_template['max_entries'];
} else {
	$i_template = 0;
	$a_template				= array();
	$s_template_name		= '';
	$s_template_file		= '';
	$i_template_max_entries	= 0;

}

//actions
//create template
$i_new_template = false;
if (isset($_POST['create_template'])) {
	$i_new_template = $oTemplate->createTemplate($_POST['name'], $_POST['file'], $_POST['max_entries']);
	if ($i_new_template) {
		header('Location: '.$oCms->adminHref('template').'&template='.$i_new_template.'&ok_created#panel_module');
		exit;
	}
}
//update template
if (isset($_POST['update_template']) && is_numeric($_POST['update_template'])) {
	if ($oTemplate->updateTemplate($_POST['update_template'], $_POST['name'], $_POST['file'], $_POST['max_entries']) === true) {
		header('Location: '.$oCms->adminHref('template').'&template='.$_POST['update_template'].'&ok_update');
		exit;
	}
}
//delete template module
if (isset($_GET['delete_template_module']) && is_numeric($_GET['delete_template_module'])) {
	if ($oTemplate->deleteTemplateModule($i_template, $_GET['delete_template_module']) === true) {
		header('Location: '.$oCms->adminHref('template').'&template='.$i_template.'&ok_deleted_template_module#panel_module');
		exit;
	} else {
		header('Location: '.$oCms->adminHref('template').'&template='.$i_template.'&error_deleted_template_module#panel_module');
		exit;
	}
}