<h2><small>Vorlagen Übersicht</small></h2>
<?php
if (isset($_GET['ok_deleted'])) {
	echo $oCms->successCallout('Die Vorlage wurde gelöscht.');
}
?>
<div class="row">
	<div class="large-12 columns">
		<div class="callout">
			<p>
				<a class="large success button" href="<?=$oCms->adminHref('template')?>&amp;create">
					<i class="fa fa-plus"></i> Vorlage hinzufügen
				</a>
			</p>
			
			<div class="row">
				<div class="large-12 columns">
<?php
if (count($oTemplate->listTemplates())) {	
?>					<table class="hover">
<?php
foreach ($oTemplate->listTemplates() as $i_template_id => $val) {	
?>
	<tr>
		<td>
			<?=$val['name']?>
		</td>
		<td>
		<div class="button-group small float-right">
			<a class="secondary button" href="<?=$oCms->adminHref('template')?>&amp;template=<?=$i_template_id?>">
				<i class="fa fa-pencil-square-o"></i> bearbeiten
			</a>
<?php
if ($oTemplate->templateIsUsedBySite($i_template_id)) {
?>
			<a class="disabled button" href="#" title="Diese Vorlage wird verwendet" onclick="return false;">
<?php
} else {
?>
			<a class="secondary button" 
			   href="<?=$oCms->adminHref('templates')?>&amp;delete_template=<?=$i_template_id?>" 
			   onclick="return (confirm('Vorlage dauerhaft löschen?')?true:false)">
<?php
}
?>
				<i class="fa fa-times"></i> löschen
			</a>
		</div>
		</td>
	</tr>
<?php
}
?>
					</table>
<?php
} else {
?>
<div class="callout secondary">
  <p>Keine Templates vorhanden</p>
</div>
<?php
}
?>
				</div>
			</div>

		</div>
	</div>
</div>
