<?php
//load classes
$oCms		= new \admin\cms();
$oTemplate	= new \app\template();

//set variables
$aModules	 = array();
$sName		 = 'testname';
$sFile		 = 'test.php';
$iMaxEntries = 0;
$aModules[]	 = array(
	'moduleName'	=> 'nameX',
	'moduleId'		=> 1,
	'moduleLength'	=> 100,
	'modulePreview'	=> 1,
	'moduleSort'	=> 1,
	'moduleOrder'	=> 0
);
$aModules[]	 = array(
	'moduleName'	=> 'lead',
	'moduleId'		=> 2,
	'moduleLength'	=> 'NULL',
	'modulePreview'	=> 0,
	'moduleSort'	=> 0,
	'moduleOrder'	=> 1
);

//action
//delete template
if (isset($_GET['delete_template']) && is_numeric($_GET['delete_template'])) {
	if ($oTemplate->deleteTemplate($_GET['delete_template'])) {
		header('Location: '.$oCms->adminHref('templates').'&ok_deleted');
		exit;
	}
}