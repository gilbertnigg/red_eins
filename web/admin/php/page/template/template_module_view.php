<?php
if (isset($_GET['create'])) {
		echo '<h2><small>Vorlage</small> '.$s_template_name.' <small><i class="fa fa-angle-right"></i> Neues Modul</small></h2>';
} else {
		echo '<h2><small>Vorlage</small> '.$s_template_name.' <small><i class="fa fa-angle-right"></i> Modul</small> '.$s_name.'</h2>';
}
?>
<div class="row">
	<div class="large-12 columns">
		<div class="callout">
<?php
if (isset($_GET['create'])) {
?>
				<form action="<?=$oCms->adminHref()?>&amp;template=<?=$i_template?>" method="post">
					<input type="hidden" name="create_template_module" value="1"><?php
} else {
?>
				<form action="<?=$oCms->adminHref()?>&amp;template=<?=$i_template?>&amp;template_module=<?=$i_template_module?>" method="post">
					<input type="hidden" name="update_template_module" value="<?=$i_template_module?>">
<?php
}
?>
					<input type="hidden" name="update_template" value="<?=$i_template?>">
						<div class="row">
							<div class="small-2 columns">
								<label for="name" class="middle">Name</label>
							</div>
							<div class="small-10 columns">
								<input type="text" name="name" id="name" aria-describedby="nameHelpText" value="<?=$s_name?>" required>
								<p class="help-text" id="nameHelpText">Ausgabe Feldname</p>
							</div>
						</div>
						<div class="row">
							<div class="small-2 columns">
								<label for="fieldname" class="middle">Feldname</label>
							</div>
							<div class="small-10 columns">
								<input type="text" name="fieldname" id="fieldname" aria-describedby="fieldnameHelpText" value="<?=$s_fieldname?>">
								<p class="help-text" id="fieldnameHelpText">Datenbank Feldname ohne Leerschläge und Umlaute</p>

							</div>
						</div>
						<div class="row">
							<div class="small-2 columns">
								<label for="module" class="middle">Modul</label>
							</div>
							<div class="small-10 columns">
								<?=$oTemplate->listTemplateModulesToSelect ('module', $s_module)?>
							</div>
						</div>
<?php
if ($i_template_max_entries!=1) {
?>
						<div class="row">
							<div class="small-2 columns">
								<label for="preview" class="middle">Vorschau</label>
							</div>
							<div class="small-10 columns">
								<?=$oTemplate->listTemplatePreviewToSelect ('preview', $b_preview)?>
							</div>
						</div>
						<div class="row">
							<div class="small-2 columns">
								<label for="sort" class="middle">Sortierung</label>
							</div>
							<div class="small-10 columns">
								<?=$oTemplate->listTemplateSortToSelect ('sort', $s_sort)?>
							</div>
						</div>
<?php
} else {
?>
								<input type="hidden" name="preview" id="preview" value="1">
								<input type="hidden" name="sort" id="sort" value="0">

<?php
}
?>
						<div class="row">
							<div class="small-12 columns text-right">
								<a href="<?=$oCms->adminHref('template')?>&amp;template=<?=$i_template?>#panel_module" class="hollow button secondary">
									zurück
								</a>
								<button type="submit" class="success button"><i class="fa fa-dot-circle-o"></i> <strong>Einstellungen sichern</strong></button>
							</div>
						</div>
					</form>
		</div>
	</div>
</div>