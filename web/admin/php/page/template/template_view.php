<?php
if (isset($_GET['create'])) {
	echo '<h2><small>neue Vorlage</small></h2>';
} else {
	echo '<h2><small>Vorlage</small> '.$s_template_name.'</h2>';
}
?>	
<?php
if (isset($_GET['ok_created'])) {
	echo $oCms->successCallout('Die Vorlage wurde erstellt.<br>Bitte fügen Sie nun der Vorlage die gewünschten Module hinzu.');
} else if (isset($_GET['ok_update'])) {
	echo $oCms->successCallout('Die Vorlage wurde aktualisiert.');
} else if (isset($_GET['ok_create_template_module'])) {
	echo $oCms->successCallout('Das Modul wurde erstellt.');
} else if (isset($_GET['ok_update_template_module'])) {
	echo $oCms->successCallout('Das Modul wurde aktualisiert.');
} else if (isset($_GET['ok_deleted_template_module'])) {
	echo $oCms->successCallout('Das Modul wurde gelöscht.');
} else if (isset($_GET['error_deleted_template_module'])) {
	echo $oCms->alertCallout('Fehler beim Löschen.');
}
?>
<div class="row">
	<div class="large-12 columns">
		<div class="callout">
<ul class="tabs" data-tabs id="template-tabs">
	<li class="tabs-title is-active">
		<a href="#panel_settings" aria-selected="true"><i class="fa fa-cog"></i> Einstellungen</a>
	</li>
<?php
if (!isset($_GET['create'])) {
?>
	<li class="tabs-title">
		<a href="#panel_module"><i class="fa fa-align-left"></i> Module</a>
	</li>
<?php
}
?>
</ul>
<div class="tabs-content" data-tabs-content="template-tabs">
	<div class="tabs-panel is-active" id="panel_settings">
		<div class="row">
			<div class="large-12 columns">				

				<form action="<?=$oCms->adminHref()?>" method="post">
<?php
if (isset($_GET['create'])) {
?>
					<input type="hidden" name="create_template" value="1"><?php
} else {
?>
					<input type="hidden" name="update_template" value="<?=$i_template?>">
<?php
}
?>
					<div class="row">
						<div class="small-2 columns">
							<label for="name" class="middle">Name</label>
						</div>
						<div class="small-10 columns">
							<input type="text" name="name" id="name" aria-describedby="nameHelpText" value="<?=$s_template_name?>" required>
							<p class="help-text" id="nameHelpText">Name der Vorlage</p>
						</div>
					</div>
					<div class="row">
						<div class="small-2 columns">
							<label for="file" class="middle">Datei</label>
						</div>
						<div class="small-10 columns"><?=$oTemplate->listTemplateFilesToSelect ('file', $s_template_file)?>
							<p class="help-text nm" id="numberHelpText">Datei der Vorlage im Ordner /php/templates/...</p>
						</div>
					</div>
					<div class="row">
						<div class="small-2 columns">
							<label for="max_entries" class="middle">Max. Einträge</label>
						</div>
						<div class="small-10 columns">
							<input type="number" name="max_entries" id="max_entries" aria-describedby="numberHelpText" value="<?=$i_template_max_entries?>" required>
							<p class="help-text" id="numberHelpText">0 = unbegrenzt</p>
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns text-right">
							<a href="<?=$oCms->adminHref('templates')?>" class="hollow button secondary">zurück</a>
							<button type="submit" class="success button"><i class="fa fa-dot-circle-o"></i> <strong>Einstellungen sichern</strong></button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
<?php
if (!isset($_GET['create'])) {
?>
<div class="tabs-panel" id="panel_module">
	<div class="row">
		<div class="large-12 columns">
			<a class="large success button" href="<?=$oCms->adminHref('template_module')?>&amp;template=<?=$i_template?>&amp;create">
				<i class="fa fa-plus"></i> Modul hinzufügen</a>
		</div>
	</div>
<?php
if (count($oTemplate->listTemplateModules($i_template))) {	
?>
	<div class="row">
		<div class="large-12 columns">
			<table id="dndTableTemplateModules" class="hover" data-template-id="<?=$i_template?>">
				<thead>
					<tr>
						<th>Name</th>
						<th>Modul</th>
<?php
if ($i_template_max_entries!=1) {
?>
						<th class="text-center">Vorschau</th>
						<th class="text-center">Sortierung</th>
<?php
}
?>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>
<?php
foreach ($oTemplate->listTemplateModules($i_template) as $key => $val) {
?>
	<tr id="module<?=$key?>">
		<td><?=$val['name']?></td>
		<td><?=$oTemplate->templateModulesType($val['module'])?></td>
<?php
if ($i_template_max_entries!=1) {
?>

		<td class="text-center"><?=$oTemplate->previewIcon($val['preview'])?></td>
		<td class="text-center"><?=$oTemplate->sortIcon($val['sort'])?></td>
<?php
}
?>
		<td>
			<div class="button-group small float-right">
				<a class="secondary button drag-handle" href="#"><i class="fa fa-bars"></i> 
					verschieben
				</a>
				<a class="secondary button" href="<?=$oCms->adminHref('template_module')?>&amp;template=<?=$i_template?>&amp;template_module=<?=$key?>">
					<i class="fa fa-pencil-square-o"></i> bearbeiten
				</a>
				<a class="secondary button" href="<?=$oCms->adminHref()?>&amp;template=<?=$i_template?>&amp;delete_template_module=<?=$key?>" onclick="return (confirm('Template Modul dauerhaft löschen?')?true:false)">
					<i class="fa fa-times"></i> löschen
				</a>
			</div>
		</td>
	</tr>
<?php
}
?>
				</tbody>
			</table>
		</div>
	</div>
<?php
} else { //if count
	echo '<p>Keine Module vorhanden</p>';
}
?>
	<div class="row">
		<div class="small-12 columns text-right">
				<a href="<?=$oCms->adminHref('templates')?>" class="hollow button secondary">zurück</a>
			</div>
		</div>
	</div>	
</div>
<?php
}
?>
		</div>
	</div>
</div>
