<?php
//load classes
$oCms		= new \admin\cms();
$oTemplate	= new \app\template();

//set variables
//TODO2: this in global cms href
$i_template = $_GET['template'];
$a_template				= $oTemplate->listTemplates($i_template);
$s_template_name		= $a_template['name'];
$s_template_file		= $a_template['file'];
$i_template_max_entries	= $a_template['max_entries'];

if (isset($_GET['template_module'])) {
	$i_template_module   = $_GET['template_module'];
	$a_module	 = $oTemplate->listTemplateModules($i_template);
	$s_name		 = $a_module[$i_template_module]['name'];
	$s_fieldname = $a_module[$i_template_module]['fieldname'];
	$s_module	 = $a_module[$i_template_module]['module'];
	$b_preview	 = $a_module[$i_template_module]['preview'];
	$s_sort		 = $a_module[$i_template_module]['sort'];
} else {
	$a_module	 = array();
	$s_name		 = '';
	$s_fieldname = '';
	$s_module	 = 1;
	$b_preview	 = 1;
	$s_sort		 = 0;
}

//actions
//create template module
if (isset($_POST['create_template_module'])) {
	$i_new_template_module = $oTemplate->createTemplateModule($_POST['update_template'], $_POST['name'], $_POST['fieldname'], $_POST['module'], $_POST['preview'], $_POST['sort']);
	if ($i_new_template_module) {
		header('Location: '.$oCms->adminHref('template').'&template='.$_POST['update_template'].'&template_module='.$i_new_template_module.'&ok_create_template_module#panel_module');
		exit;
	}
}

//update template module
if (isset($_POST['update_template_module']) && is_numeric($_POST['update_template_module'])) {
	if ($oTemplate->updateTemplateModule($_POST['update_template'], $_POST['update_template_module'], $_POST['name'], $_POST['fieldname'], $_POST['module'], $_POST['preview'], $_POST['sort']) === true) {
		header('Location: '.$oCms->adminHref('template').'&template='.$_POST['update_template'].'&template_module='.$_POST['update_template_module'].'&ok_update_template_module#panel_module');
		exit;
	echo "aa";
	}
	echo "bb";
}