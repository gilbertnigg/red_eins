<?php
//load classes
$oCms		= new \admin\cms();
$oSite		= new \app\site();
$oContent	= new \app\content();
$oTemplate	= new \app\template();

//set variables
$oPreviewFields = $oContent->listContentPreviewFields();

//actions
//delete content
$action_delete = false;
if (isset($_GET['delete_content']) && is_numeric($_GET['delete_content'])) {
	$action_delete = $oContent->deleteContent(NAVID, $_GET['delete_content']);
	$aRows = $oContent->itemIds();
	if ($action_delete === true) {
		if (!count($aRows)) {
			header('Location: '.$oCms->adminHref('sites').'&navid='.NAVID.'&ok_deleted');
		} else {
			header('Location: '.$oCms->adminHref('contents').'&navid='.NAVID.'&ok_deleted');
		}
		exit;
	}
}

//get max entries from associated template
$a_site_info			= $oSite->siteInfo();
$s_name_navigation		= $a_site_info['name_navigation'];
$i_template				= $a_site_info['template'];
$a_template				= $oTemplate->listTemplates($i_template);
$i_template_max_entries	= $a_template['max_entries'];

//redirects
$aRows = $oContent->itemIds();
if (!count($aRows)) {
	//direct to new item if no items stored
	header('Location: '.$oCms->adminHref('content').'&navid='.NAVID.'&create');
	exit;	
} else if ($i_template_max_entries==1) {
	//direct to item if max 1 item and item is stored
	header('Location: '.$oCms->adminHref('content').'&navid='.NAVID.'&itemid='.$aRows[0]);
	exit;	
}