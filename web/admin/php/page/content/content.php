<?php
//load classes
$oCms			= new \admin\cms();
$oSite			= new \app\site();
$oTemplate		= new \app\template();
$oContent		= new \app\content();
$oContentEdit	= new \admin\contentedit();

//set variables
$aContentFields = $oContent->listContentFields();

//get max entries from associated template
$a_site_info = $oSite->siteInfo();
$s_name_navigation  = $a_site_info['name_navigation'];
$i_template  = $a_site_info['template'];
$a_template	 = $oTemplate->listTemplates($i_template);
$i_template_max_entries	= $a_template['max_entries'];

//actions
$action_create = false;
if (isset($_POST['-create_content'])) {
	$action_create = $oContent->createContent(
		$_POST
	);
	if (is_numeric($action_create)) {
		if ($i_template_max_entries==1) {
			header('Location: '.$oCms->adminHref('sites').'&navid='.NAVID.'&ok_create');
		} else {
			header('Location: '.$oCms->adminHref('contents').'&navid='.NAVID.'&itemid='.$action_create.'&ok_create');
		}
		exit;
	}
}

$action_update = false;
if (isset($_POST['-update_content']) && is_numeric($_POST['-update_content'])) {
	$action_update = $oContent->updateContent(
		$_POST, $_POST['-update_content']
	);
	if ($action_update === true) {
		if ($i_template_max_entries==1) {
			header('Location: '.$oCms->adminHref('sites').'&navid='.NAVID.'&ok_update');
		} else {
			header('Location: '.$oCms->adminHref('contents').'&navid='.NAVID.'&itemid='.$_POST['-update_content'].'&ok_update');
		}
		exit;
	}
}

//require_once('content_view.php');
/*
$action_delete = false;
if (isset($_GET['delete_site']) && is_numeric($_GET['delete_site'])) {
	$action_delete = $oTemplate->deleteTemplateModule($i_template, $_GET['delete_template_module'];
	if ($action_delete) === true) {
		header('Location: '.$oCms->adminHref('template').'&template='.$i_template.'&ok_deleted#panel_module');
		exit;
	}
}*/
