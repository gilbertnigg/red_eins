<h2><small>Inhalt</small> <?=$s_name_navigation?></h2>
<script src="bower_components/tinymce/tinymce.min.js"></script>
<div class="row">
	<div class="large-12 columns">
		<div class="callout">
		<form action="<?=$oCms->adminHref()?>&amp;navid=<?=NAVID?>&amp;itemid=<?=ITEMID?>" 
			  method="post" enctype="multipart/form-data" accept-charset="utf-8" >
<?php
if (isset($_GET['create'])) {
?>
			<input type="hidden" name="-create_content" value="1"><?php
} else {
?>
			<input type="hidden" name="-update_content" value="<?=ITEMID?>">
<?php
}
?>			
			<div class="row">
				<div class="large-12 columns">
					<table class="hover v-headers">


<?php
//contents
foreach ($aContentFields as $a_field_value) {
	$s_name			 = $a_field_value['name'];
	$s_fieldname	 = $a_field_value['fieldname'];
	$i_module		 = $a_field_value['module'];
	$s_field_content = $oContent->fieldContent($s_fieldname);
	if ($s_fieldname[0] != '_') {
		echo $oContentEdit->getModuleRow ($i_module, $s_name, $s_fieldname, $s_field_content);
	}
}
?>
					</table>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns text-right">
				<a href="<?=$oCms->adminHref('contents')?>&amp;navid=<?=NAVID?>" class="hollow button secondary">zurück</a>
				<button type="submit" class="success button"><i class="fa fa-dot-circle-o"></i> <strong>Inhalt sichern</strong></button>
				</div>
			</div>
		</form>
		</div>
	</div>
</div>
