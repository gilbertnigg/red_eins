<h2><small>Inhalte </small> <?=$s_name_navigation?></h2>
<?php
if (isset($_GET['ok_create'])) {
	echo $oCms->successCallout('Der Inhalt wurde hinzugefügt.');
} else if (isset($_GET['ok_update'])) {
	echo $oCms->successCallout('Der Inhalt wurde aktualisiert.');
} else if (isset($_GET['ok_deleted'])) {
	echo $oCms->successCallout('Der Inhalt wurde gelöscht.');
}
?>

<div class="row">
	<div class="large-12 columns">
		<div class="callout">
			<p>
				<a class="large success button" href="<?=$oCms->adminHref('content')?>&amp;navid=<?=NAVID?>&amp;create"><i class="fa fa-plus"></i> Inhalt hinzufügen</a>
			</p>
			
			<div class="row">
				<div class="large-12 columns">
					<table id="contentsTable" class="hover">
						<thead>
							<tr>
<?php
foreach ($oPreviewFields as $a_field_info) {
?>
								<th><?=$a_field_info['name']?></th>
<?php
}
?>
								<th class="no-sort">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
<?php
foreach ($oContent->itemIds ()  as $i_item_id) {
?>
							<tr>
<?php
foreach ($oContent->listContentPreview(NAVID, LANG) as $s_field_name => $val_field) {
?>
								<td>
									<?=$oContent->fieldContent($s_field_name, $i_item_id)?>
								</td>
<?php
}
?>
								<td>
									<div class="button-group small float-right">
<!--
TODO2: manuell verschieben
										<a class="secondary button" href="#"><i class="fa fa-bars"></i> verschieben</a> -->
										<a class="secondary button" href="<?=$oCms->adminHref('content')?>&amp;navid=<?=NAVID?>&amp;itemid=<?=$i_item_id?>"><i class="fa fa-pencil-square-o"></i> bearbeiten</a>
										<a class="secondary button" href="<?=$oCms->adminHref('contents')?>&amp;navid=<?=NAVID?>&amp;delete_content=<?=$i_item_id?>" onclick="return (confirm('Inhalt dauerhaft löschen?')?true:false)"><i class="fa fa-times"></i> löschen</a>
									</div>
								</td>
							</tr>
<?php
}
?>
						</tbody>
					</table>
				</div>
			</div>
			
			<div class="row">
				<div class="small-12 columns text-right">
				<a href="<?=$oCms->adminHref('sites')?>&amp;navid=<?=NAVID?>" class="hollow button secondary">zurück</a>
				</div>
			</div>
		</div>
	</div>
</div>
