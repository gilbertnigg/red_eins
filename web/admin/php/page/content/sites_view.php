<h2><small>Inhalte Übersicht</small></h2>
<?php
if (isset($_GET['ok_create'])) {
	echo $oCms->successCallout('Der Inhalt wurde hinzugefügt.');
} else if (isset($_GET['ok_update'])) {
	echo $oCms->successCallout('Der Inhalt wurde aktualisiert.');
} else if (isset($_GET['ok_deleted'])) {
	echo $oCms->successCallout('Der Inhalt wurde gelöscht.');
}
?>
<div class="row">
	<div class="large-12 columns">
<?php
if (count($oSite->tree())) {
?>
		<div class="callout">
			<div class="row">
				<div id="jstreeContent" class="large-12 columns">
					<?=$oContent->treeNavigation($aSites)?>
				</div>
			</div>
		</div>
<?php		
} else {
?>
<div class="callout secondary">
  <p>Keine Seiten vorhanden</p>
</div>
<?php
}
?>
	</div>
</div>
