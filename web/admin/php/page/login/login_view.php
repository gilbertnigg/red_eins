<div class="row">
	<div class="large-6 columns large-centered">				
		<h2><br>Willkommen</h2>
		<form action="<?=$oCms->adminHref('login', 'login')?>" method="post">
			<div class="row">
				<div class="small-4 columns">
					<label for="name" class="middle">Name</label>
				</div>
				<div class="small-8 columns">
					<input type="text" name="name" id="name" value="template-admin">
				</div>
			</div>
			<div class="row">
				<div class="small-4 columns">
					<label for="password" class="middle">Kennwort</label>
				</div>
				<div class="small-8 columns">
					<input type="password" name="password" id="password" value="Pass123">
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns text-right">
					<input type="submit" class="success button" value="Login">
				</div>
			</div>
		</form>
		<code>Name: template-admin Kennwort: Pass123</code><br>
		<code>Name: site-admin &nbsp;&nbsp;&nbsp;&nbsp;Kennwort: Pass123</code><br>
		<code>Name: content-admin &nbsp;Kennwort: Pass123</code>
	</div>
</div>