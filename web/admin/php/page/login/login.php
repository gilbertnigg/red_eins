<?php
//load classes
$oCms	= new \admin\cms();
$oUser	= new \admin\user();

//set variables
$i_login = false;

//redirect if login site
if ($oUser->checkLogin()) {
	$i_login = true;
}

//login and redirect if login is correct
if (isset($_POST['name']) && isset($_POST['password'])) {
	$s_name		= htmlspecialchars($_POST['name']);
	$s_password = htmlspecialchars($_POST['password']);
	if ($oUser->login($s_name, $s_password) === true) {
		$i_login = true;
	}
}

if ($i_login===true) {
	$aModules      = $oUser->getUserModules();
	$s_startmodule = $oUser->getUserStartModule();
	$s_startpage   = $aModules[$oUser->getUserStartModule()]['startpage'];
	header('Location: '.$oCms->adminHref($s_startpage, $s_startmodule));
	exit;
}