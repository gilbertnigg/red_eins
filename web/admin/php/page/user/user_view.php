<ul class="tabs" data-tabs id="example-tabs">
	<li class="tabs-title is-active"><a href="#panel1" aria-selected="true">Kennwort</a></li>
	<li class="tabs-title"><a href="#panel2">Benutzername &amp; E-Mail</a></li>
</ul>

<div class="tabs-content" data-tabs-content="example-tabs">
	<div class="tabs-panel is-active" id="panel1">
		<div class="row">
			<div class="large-6 columns large-centered">				
				<form>
					<div class="row">
						<div class="small-4 columns">
							<label for="psw_o" class="middle">Aktuelles Kennwort</label>
						</div>
						<div class="small-8 columns">
							<input type="password" id="psw_o">
						</div>
					</div>
					<div class="row">
						<div class="small-4 columns">
							<label for="psw_n1" class="middle">Neues Kennwort</label>
						</div>
						<div class="small-8 columns">
							<input type="password" id="psw_n1">
						</div>
					</div>
					<div class="row">
						<div class="small-4 columns">
							<label for="psw_n2" class="middle">Kennwort wiederholen</label>
						</div>
						<div class="small-8 columns">
							<input type="password" id="psw_n2">
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns text-right">
							<button type="submit" class="success button" onclick="alert('Demo-Mode: User settings can not be saved');return false;"><i class="fa fa-dot-circle-o"></i> <strong>Kennwort sichern</strong></button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="tabs-panel" id="panel2">
		<div class="row">
			<div class="large-6 columns large-centered">
				<form>
					<div class="row">
						<div class="small-4 columns">
							<label for="usr" class="middle">Benutzername</label>
						</div>
						<div class="small-8 columns">
							<input type="text" id="usr" placeholder="Loremipsum" disabled="disabled" title="Benutzername kann nicht geändert werden">
						</div>
					</div>
					<div class="row">
						<div class="small-4 columns">
							<label for="email" class="middle">E-Mail</label>
						</div>
						<div class="small-8 columns">
							<input type="text" id="email" value="lorem@ipsum.ch">
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns text-right">
							<button type="submit" class="success button" onclick="alert('Demo-Mode: User settings can not be saved');return false;"><i class="fa fa-dot-circle-o"></i> <strong>Einstellungen sichern</strong></button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
