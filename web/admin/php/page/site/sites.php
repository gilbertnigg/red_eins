<?php
//load classes
$oCms	= new \admin\cms();
$oSite	= new \app\site();

//set variables
$aSites = $oSite->tree();

//actions
if (isset($_GET['delete_site']) && is_numeric($_GET['delete_site'])) {
	if ($oSite->deleteSiteAndSubSites($_GET['delete_site'])) {
		header('Location: '.$oCms->adminHref('sites').'&ok_deleted');
		exit;
	}
}