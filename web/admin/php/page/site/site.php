<?php
//load classes
$oCms	   = new \admin\cms;
$oSite	   = new \app\site;
$oTemplate = new \app\template;

//set variables
$aSites = $oSite->tree();

foreach (\app\lang::$languages as $key => $val) {
	$s_lang_code = $val['langCode'];
	$aSiteInfo[$s_lang_code] = $oSite->siteInfo(NAVID, $s_lang_code);
}

//html helpers
if (isset($_GET['create'])) {
	$s_input_parent		  = '<input type="hidden" name="parent" value="'.(isset($_GET['parent'])?$_GET['parent']:0).'">';
	$s_input_position	  = '<input type="hidden" name="position" value="0">';
} else {
	$s_input_parent		  = '<input type="hidden" name="parent" value="'.$aSiteInfo[LANG]['_parent'].'">';
	$s_input_position	  = '<input type="hidden" name="position" value="'.$aSiteInfo[LANG]['_order'].'">';
}

					   
//actions
$action_create = false;
if (isset($_POST['create_site'])) {
	foreach (\app\lang::$languages as $key => $a_val) {
		$s_lang_code = $a_val['langCode'];
		$aNameNavigation[$s_lang_code]	= $_POST['name_navigation_'.$s_lang_code];
		$aNameUrl[$s_lang_code]			= $_POST['name_url_'.$s_lang_code];
		$aMetaTitle[$s_lang_code]		= $_POST['meta_title_'.$s_lang_code];
		$aMetaDescription[$s_lang_code]	= $_POST['meta_description_'.$s_lang_code];
		$aMetaKeywords[$s_lang_code]	= $_POST['meta_keywords_'.$s_lang_code];
		$aVisibility[$s_lang_code]		= $_POST['visibility_'.$s_lang_code];
	}
	$action_create = $oSite->createSite(
		//TODO3: wahl zwischen anfang oder ende einfügen
		$_POST['template'], $_POST['redirect'], $_POST['parent'], $_POST['position'],
		$aNameNavigation, $aNameUrl, $aMetaTitle, $aMetaDescription, $aMetaKeywords, $aVisibility
	);
	if (is_numeric($action_create)) {
		header('Location: '.$oCms->adminHref('sites').'&navid='.$action_create.'&ok_create');
		exit;
	}
}

$action_update = false;
if (isset($_POST['update_site']) && is_numeric($_POST['update_site'])) {
	foreach (\app\lang::$languages as $key => $a_val) {
		$s_lang_code = $a_val['langCode'];
		$aNameNavigation[$s_lang_code]	= $_POST['name_navigation_'.$s_lang_code];
		$aNameUrl[$s_lang_code]			= $_POST['name_url_'.$s_lang_code];
		$aMetaTitle[$s_lang_code]		= $_POST['meta_title_'.$s_lang_code];
		$aMetaDescription[$s_lang_code]	= $_POST['meta_description_'.$s_lang_code];
		$aMetaKeywords[$s_lang_code]	= $_POST['meta_keywords_'.$s_lang_code];
		$aVisibility[$s_lang_code]		= $_POST['visibility_'.$s_lang_code];
	}
	$action_update = $oSite->updateSite(
		$_POST['update_site'], $_POST['template'], $_POST['redirect'], $_POST['parent'], $_POST['position'], 
		$aNameNavigation, $aNameUrl, $aMetaTitle, $aMetaDescription, $aMetaKeywords, $aVisibility
	);
	if ($action_update === true) {
		header('Location: '.$oCms->adminHref('sites').'&navid='.$_POST['update_site'].'&ok_update');
		exit;
	}
}

$action_delete = false;
if (isset($_GET['delete_site']) && is_numeric($_GET['delete_site'])) {
	$action_delete = $oTemplate->deleteTemplateModule($i_template, $_GET['delete_template_module']);
	if ($action_delete === true) {
		header('Location: '.$oCms->adminHref('template').'&template='.$i_template.'&ok_deleted#panel_module');
		exit;
	}
}