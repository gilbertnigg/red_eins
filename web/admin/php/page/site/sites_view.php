<div class="row">
	<div class="large-12 columns">
		<h2><small>Seiten Übersicht</small></h2>
<?php
if (isset($_GET['ok_create'])) {
	echo $oCms->successCallout('Die Seite wurde erstellt.');
} else if (isset($_GET['ok_update'])) {
	echo $oCms->successCallout('Die Seite wurde aktualisiert.');
} else if (isset($_GET['ok_deleted'])) {
	echo $oCms->successCallout('Die Seite wurde gelöscht.');
}
?>
		<div class="callout">
			<p>
					<a class="large success button" href="<?=$oCms->adminHref('site')?>&amp;navid=0&amp;parent=0&amp;create">
						<i class="fa fa-plus"></i> Seite hinzufügen</a>
					<a class="large button" href="<?=$oCms->adminHref('site_global')?>&amp;action=update">
						<i class="fa fa-info-circle"></i> Globale Einstellungen</a>
				</p>
<?php
if (count($oSite->tree())) {
?>
			<div class="row">
				<div id="jstreeSite" class="large-12 columns" data-langcode="<?=LANG?>">
					<?=$oSite->treeNavigation($aSites)?>
				</div>
			</div>
<?php
} else {
?>
<div class="callout secondary">
  <p>Keine Seiten vorhanden</p>
</div>
<?php
}
?>
		</div>
	</div>
</div>