<div class="row">
	<div class="large-12 columns">
		<h2><small>Seite</small> <?=$aSiteInfo[LANG]['name_navigation']?></h2>
		<div class="callout">

<ul class="tabs" data-tabs id="example-tabs">
<?php
foreach (\app\lang::$languages as $s_key => $a_val) {
	$s_lang_code = $a_val['langCode'];
?>
	<li class="tabs-title<?=(!$s_key?' is-active':'')?>"><a href="#panel_<?=$s_lang_code?>"><?=strtoupper($s_lang_code)?></a></li>
<?php
}
?>
	<li class="tabs-title"><a href="#panel3"><i class="fa fa-cog"></i> Einstellungen</a></li>
</ul>

<form action="<?=$oCms->adminHref()?>&amp;navid=<?=NAVID?>" method="post">
<?php
if (isset($_GET['create'])) {
?>
	<input type="hidden" name="create_site">
<?php
} else {
?>
	<input type="hidden" name="update_site" value="<?=NAVID?>">
<?php
}
?>
	<div class="tabs-content" data-tabs-content="example-tabs">
<?php
$aLanguages = \app\lang::$languages;
foreach ($aLanguages as $s_key => $a_val) {
	$s_lang_code = $a_val['langCode'];
?>
		<div class="tabs-panel<?=(!$s_key?' is-active':'')?>" id="panel_<?=$s_lang_code?>">
			<div class="row">
				<div class="large-12 columns">
					<div class="row">
						<div class="small-2 columns">
							<label for="name_navigation_<?=$s_lang_code?>" class="middle">Name Navigation</label>
						</div>
						<div class="small-10 columns">
							<input type="text" name="name_navigation_<?=$s_lang_code?>" id="name_navigation_<?=$s_lang_code?>" value="<?=$aSiteInfo[$s_lang_code]['name_navigation']?>" required>
					</div>
					</div>
					<div class="row">
						<div class="small-2 columns">
							<label for="name_url_<?=$s_lang_code?>" class="middle">Name URL</label>
						</div>
						<div class="small-10 columns">
							<input type="text" name="name_url_<?=$s_lang_code?>" id="name_url_<?=$s_lang_code?>" value="<?=$aSiteInfo[$s_lang_code]['name_url']?>">
							<p class="help-text">Name URL ohne Sonderzeichen und Leerschläge</p>
						</div>
					</div>
					<div class="row">
						<div class="small-2 columns">
							<label for="visibility_<?=$s_lang_code?>" class="middle">Sichtbarkeit</label>
						</div>
						<div class="small-10 columns">
							<?=$oSite->listVisibilityToSelect ('visibility_'.$s_lang_code, $aSiteInfo[$s_lang_code]['visibility'])?>
						</div>
					</div>
					<div class="row">
						<div class="small-2 columns">
							<label for="meta_title_<?=$s_lang_code?>" class="middle">Meta Titel</label>
						</div>
						<div class="small-10 columns">
							<input type="text" name="meta_title_<?=$s_lang_code?>" id="meta_title_<?=$s_lang_code?>" value="<?=$aSiteInfo[$s_lang_code]['meta_title']?>">
						</div>
					</div>
					<div class="row">
						<div class="small-2 columns">
							<label for="meta_description_<?=$s_lang_code?>" class="middle">Meta Beschreibung</label>
						</div>
						<div class="small-10 columns">
							<input type="text" name="meta_description_<?=$s_lang_code?>" id="meta_description_<?=$s_lang_code?>" value="<?=$aSiteInfo[$s_lang_code]['meta_description']?>">
						</div>
					</div>
					<div class="row">
						<div class="small-2 columns">
							<label for="meta_keywords_<?=$s_lang_code?>" class="middle">Meta Schlüsselwörter</label>
						</div>
						<div class="small-10 columns">
							<input type="text" name="meta_keywords_<?=$s_lang_code?>" id="meta_keywords_<?=$s_lang_code?>" value="<?=$aSiteInfo[$s_lang_code]['meta_keywords']?>">
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns text-right">
							<a href="<?=$oCms->adminHref('sites')?>" class="hollow button secondary">zurück</a>
							<button type="submit" class="success button"><i class="fa fa-dot-circle-o"></i> <strong>Einstellungen sichern</strong></button>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php
}
?>
	<div class="tabs-panel" id="panel3">
		<div class="row">
			<div class="large-12 columns">				
					<div class="row">
						<div class="small-2 columns">
							<label for="template" class="middle">Vorlage</label>
						</div>
						<div class="small-10 columns">
							<?=$oTemplate->listTemplatesToSelect ('template', $aSiteInfo[LANG]['template'], (isset($_GET['create'])?'create':'update'))?>
							<?=$s_input_parent?>
							<?=$s_input_position?>
						</div>
					</div>
					<div class="row">
						<div class="small-2 columns">
							<label for="redirect" class="middle">Weiterleitung</label>
						</div>
						<div class="small-10 columns">
							<?=$oSite->listRedirectsToSelect('redirect', $aSiteInfo[LANG]['redirect'], $aSites)?>
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns text-right">
							<a href="<?=$oCms->adminHref('sites')?>" class="hollow button secondary">zurück</a>
							<button type="submit" class="success button"><i class="fa fa-dot-circle-o"></i> <strong>Einstellungen sichern</strong></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
		</div>
	</div>
</div>


