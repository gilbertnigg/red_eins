<?php
require '../../../php/config/config.php';
require '../../../php/class/autoload.php';

//load classes
$oSecuresession = new \admin\securesession;
$oDb			= new \app\db($host, $username, $password, $database);
$oSettings		= new \admin\settings($a_conf);
$oLang			= new \app\lang;
$oUser			= new \admin\user();
$oCms			= new \admin\cms();
$oSite			= new \app\site();

if (isset($_POST['action'])) {
	if ($_POST['action']='dnd_move') {
		$i_navid	= str_replace('item', '', $_POST['navid']);
		$i_parent	= str_replace('item', '', $_POST['parent']);
		$i_order	= $_POST['order'];
		$s_langcode = $_POST['langcode'];
		if ($oSite->reOrderSite($i_navid, $i_parent, $i_order, $s_langcode)) {
			echo "OK";
			return true;
		}
	}
}