<?php
require '../../../php/config/config.php';
require '../../../php/class/autoload.php';

//load classes
$oSecuresession = new \admin\securesession;
$oDb			= new \app\db($host, $username, $password, $database);
$oSettings		= new \admin\settings($a_conf);
$oLang			= new \app\lang;
$oUser			= new \admin\user();
$oCms			= new \admin\cms();
$oTemplate		= new \app\template();

if (isset($_POST['action'])) {
	if ($_POST['action'] = 'dnd_sort') {
		$i_template_id	= $_POST['templateid'];
		$s_moduleorder	= $_POST['moduleorder'];
		if ($oTemplate->reOrderTemplateModules($i_template_id, $s_moduleorder)) {
			echo "OK";
			return true;
		}
	}
}