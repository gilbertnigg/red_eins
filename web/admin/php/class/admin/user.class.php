<?php
//TODO2: 
//- navigation access allowed sites
namespace admin;

class user  {
	protected $DB = false;
	public function __construct() {
		$aConf			  = \admin\statics::$aConf;
		$this->secret_key = $aConf['secret_key'];
		$this->DB		  = \app\db::load();
		if (isset($_GET['logout'])) {
			$this->logout();
		}
	}
		
	/**
	 * Create user
	 *
	 * @access	public
	 * @param	string $name		User name
	 * @param	string $password	User password uncrypted
	 * @param	string $email		User email
	 * @param	string $type		User administration type {
		 								0 = Content-Editor
		 								1 = Site-Editor
		 								2 = Template-Editor	 
		 								// TODO2:
										//3 = Meta-Editor
	 								}									
	 * @uses	db
	 * @uses	$secret_key
	 * @return	integer				New user id
	 */
	public function create($name, $password, $email='', $type=0) {
		//encrypt password for saving
		$password_hash = password_hash($password.$this->secret_key, PASSWORD_DEFAULT);
		
		//insert db
		$stmt = $this->DB->prepare('
			INSERT INTO `users` 
			(`name`, `email`, `password`, `type`, `_create_time`) 
			VALUES (?, ?, ?, ?, CURRENT_TIMESTAMP)
		');
		$stmt->bind_param(
			'sssss', 
			$name, $email, $password_hash, $type
		);
		$stmt->execute();
		$stmt->close();
		return $this->DB->insert_id;
	}
	
	/**
	 * User login
	 *
	 * @access	public
	 * @param	string $name		User name
	 * @param	string $password	User password uncrypted

	 * @uses	db
	 * @uses	$secret_key
	 * @return	string = error || boolean true = ok
	 */
	 public function login($name, $password) {
		 //TODO2: submit form password encrypted
		//prevent XSS
		$name = \app\text::clearText($name, false, false, true);
		//TODO2: check password string
		//$password = preg_replace('/[^a-zA-Z0-9_\-\+\"\*\%\&\/\(\)\=\?\'\.\,\#]+/', '', $password);
		if (!$name || !$password) {
			return 'Geben Sie Benutzername und Kennwort ein';
		}
		//get password from db
		$stmt = $this->DB->prepare('
			SELECT password, _id FROM `users` WHERE `name`=? LIMIT 1
		');
		$stmt->bind_param('s', $name);
		$stmt->execute();
		$stmt->store_result();
		//no such user found
		if (!$stmt->num_rows) {
			return 'Benutzername wurde nicht gefunden';
		}
		$stmt->bind_result($db_password, $id);
		$stmt->fetch();
		$stmt->close();
		//verify password
		if (password_verify($password.$this->secret_key, $db_password)) {
			//update db, set new password hash
			$password_hash = password_hash($password.$this->secret_key, PASSWORD_DEFAULT);
			$stmt = $this->DB->prepare('
				UPDATE `users` 
				SET `password`=?, `_login_time`=CURRENT_TIMESTAMP 
				WHERE `_id`=? LIMIT 1
			');
			$stmt->bind_param('si', $password_hash, $id);
			$stmt->execute();
			$stmt->close();
			//all done correctly
			//set login session
			$_SESSION['_red_']['user']['name'] = $name;
			$user_browser = $_SERVER['HTTP_USER_AGENT'];
			$_SESSION['_red_']['user']['hash'] = password_hash($password_hash.$user_browser, PASSWORD_DEFAULT);
			return true;
		} else {
			return 'Benutzername oder Kennwort wurden nicht korrekt angegeben';
		}
	}
	
	/**
	 * Check if user is logged in 
	 *
	 * @access	public
	 * @uses	db
	 * @uses	$_SESSION['_red_']
	 * @return	boolean
	 */
	public function checkLogin() {
		if (isset($_SESSION['_red_']['user']['name']) && isset($_SESSION['_red_']['user']['hash'])) {
			$name = $_SESSION['_red_']['user']['name'];
			$name = \app\text::clearText($name, false, false, true);
			$stmt = $this->DB->prepare('
				SELECT password FROM `users` WHERE `name`=? LIMIT 1
			');
			$stmt->bind_param('s', $name);
			$stmt->execute();
			$stmt->store_result();
			//no such user found
			if (!$stmt->num_rows) {
				return false;
			}
			$stmt->bind_result($db_password);
			$stmt->fetch();
			$stmt->close();
			$user_browser = $_SERVER['HTTP_USER_AGENT'];
			if (password_verify($db_password.$user_browser, $_SESSION['_red_']['user']['hash'])) {
				return true;
			}
		}
		//not correctly logged in
		return false;
	}
	
	/**
	 * Update user
	 *
	 * @access	public
	 * @param	string $name				User name
	 * @param	string $old_password		Current user password uncrypted
	 * @param	string $new_password		New user password unencrypted
	 * @param	string $new_password_repeat	Repeat new user password unencrypted
	 * @param	integer $id					User id
	 * @param	string $email				User email-address
	 * @uses	db
	 * @uses	$secret_key
	 * @return	string = error ||  boolean true = ok
	 */
	public function update($name, $old_password, $new_password, $new_password_repeat, $id, $email='') {
		//$name = preg_replace('/[^a-zA-Z0-9_\-]+/', '', $name);
		//$id = preg_replace('/[^0-9]+/', '', $id);
		//update password
		if ($old_password || $new_password || $new_password_repeat) {
			//compare passwords
			if (!$old_password) {
				return 'Geben Sie Ihr aktuelles Passwort ein';
			}
			if ($new_password != $new_password_repeat) {
				return 'Ihre eingegebenen Passwörter sind nicht identisch';
			}
			if (!$new_password || !$new_password_repeat) {
				return 'Bestätigen Sie Ihr eingegebenes Passwort';
			}
			//get current password from db
			$stmt = $this->DB->prepare('
				SELECT password FROM `users` WHERE `_id`=? LIMIT 1
			');
			$stmt->bind_param('i', $id);
			$stmt->execute();
			$stmt->store_result();
			//no such user found
			if (!$stmt->num_rows) {
				return false;
			}
			$stmt->bind_result($db_password);
			$stmt->fetch();
			$stmt->close();
			//verify password
			if (!password_verify($old_password.$this->secret_key, $db_password)) {
				return 'Ihr aktuelles Passwort wurde nicht korrekt eingegeben';
			}
			//encrypt password for saving
			$password_hash = password_hash($new_password.$this->secret_key, PASSWORD_DEFAULT);
			//update db
			$stmt = $this->DB->prepare('
				UPDATE `users` 
				SET `name`=? , `password`=?, `email`=?, `_update_time`=CURRENT_TIMESTAMP
				WHERE `_id`=? LIMIT 1
			');
			$stmt->bind_param('sssi', $name, $password_hash, $email, $id);
			$stmt->execute();
			$stmt->close();
		}
		//update name
		if ($name) {
			$stmt = $this->DB->prepare('
				UPDATE `users` 
				SET `name`=?, `_update_time`=CURRENT_TIMESTAMP
				WHERE `_id`=? LIMIT 1
			');
			$stmt->bind_param('si', $name, $id);
			$stmt->execute();
			$stmt->close();
		}
		//update email-address
		if ($email) {
			$stmt = $this->DB->prepare('
				UPDATE `users` 
				SET `email`=?, `_update_time`=CURRENT_TIMESTAMP
				WHERE `_id`=? LIMIT 1
			');
			$stmt->bind_param('si', $email, $id);
			$stmt->execute();
			$stmt->close();
		}
		//all done correctly
		return true;
	}
	
	/**
	 * Delete user
	 *
	 * @access	public
	 * @param	integer $id		User id
	 * @uses	db
	 * @return	boolean
	 */
	public function delete($id) {
		$id = preg_replace('/[^0-9]+/', '', $id);
		$stmt = $this->DB->prepare('
			SELECT password FROM `users` WHERE `_id`=? LIMIT 1
		');
		$stmt->bind_param('i', $id);
		$stmt->execute();
		$stmt->store_result();
		//no such user found
		if (!$stmt->num_rows) {
			return false;
		}
		$stmt->close();
		//delete
		$stmt = $this->DB->prepare('
			DELETE FROM users WHERE _id=? LIMIT 1
		');
		$stmt->bind_param('i', $id);
		$stmt->execute();
		$stmt->close();
		return true;
	}
	
	/**
	 * Get ID from logged-in user
	 *
	 * @access	public
	 * @param	string		session
	 * @return	boolean | integer
	 */
	public function getUserId () {
		if (empty($_SESSION['_red_']['user']['name'])) return 0;
		$sUserName = \app\text::clearText($_SESSION['_red_']['user']['name'], false, false, true);
		$sQuery	   = 'SELECT _id FROM `users` WHERE `name`="'.$sUserName.'"';
		if (!$result = $this->DB->query($sQuery)) return 0;
		$row	 = $result->fetch_object();
		$iUserId = $row->_id;
		return $iUserId;
	}
	
	/**
	 * Get allowed cms modules for logged-in user
	 *
	 * @access	public
	 * @param	string		session
	 * @return	array
	 */
	public function getUserModules () {
		$iUserType = $this->DB->getOne('users', $this->getUserId(), 'type');
		$_aModules = \admin\statics::$aConf['modules'];
		switch ($iUserType) {
			case 1:
				$aModules = array('template'=>$_aModules['template'], 'site'=>$_aModules['site'], 'content'=>$_aModules['content']);
				break;
			case 2:
				$aModules = array('site'=>$_aModules['site'], 'content'=>$_aModules['content']);
				break;
			default:
				$aModules = array('content'=>$_aModules['content']);
				break;
		}
		return $aModules;
	}
	
	/**
	 * Get startmodule for logged-in user
	 *
	 * @access	public
	 * @param	string		session
	 * @return	string
	 */
	public function getUserStartModule () {
		$sStartModule = $this->DB->getOne('users', $this->getUserId(), 'startmodule');
		return $sStartModule;
	}
	
	/**
	 * Get URL for home
	 * @return	html string
	 */
	public function homeUrl() {
		$aModules     = $this->getUserModules();
		$sStartmodule = $this->getUserStartModule();
		$sStartpage   = $aModules[$this->getUserStartModule()]['startpage'];
		$sUrl		  = \admin\cms::adminHref($sStartpage, $sStartmodule);
		return $sUrl;
	}
	
	/**
	 * Logout user
	 *
	 * @access	public
	 * @param	boolean	$redirect	Redirect to URL after logout
	 * @return	redirect to login
	 */
	public function logout() {
		if (isset($_SESSION['_red_'])) {
			unset($_SESSION['_red_']);
		}
		header('Location: '.\admin\cms::adminHref('login', 'login'));
		exit;
	}
}
