<?php
namespace admin;

class cms  {
	protected $DB = false;
	public function __construct() {
		$this->DB = \app\db::load();
	}
	
	/**
	 * Load required pages for index file
	 * @uses	db
	 * @loads php controller- and view-files or 404 redirect
	 */
	public function loadPages($sModuleName, $sPageName) {
		$s_page_controller = ADMIN_BASEDIR.'php/page/'.$sModuleName.'/'.$sPageName.'.php';
		$s_page_view	   = ADMIN_BASEDIR.'php/page/'.$sModuleName.'/'.$sPageName.'_view.php';
		if (is_file($s_page_controller) && is_file($s_page_controller)) {
			require_once($s_page_controller);
			require_once($s_page_view);
		} else {
			//404
			header('Location: '.\admin\cms::adminHref('login', 'login'));
			exit;
		}
	}
	
	/**
	 * Create language navigation
	 * @return	html string
	 */
	public function languageNavigation() {
		if (MODULE=='content' && count(\app\lang::$languages)>1) {
			$sReturn = '';
			foreach (\app\lang::$languages as $aVal) {
				$sReturn.= 
'<a href="'.\admin\cms::adminHref(PAGE, MODULE, $aVal['langCode']).'&navid='.NAVID.'" '.
'class="button lang-flag'.(LANG==$aVal['langCode']?' selected':'').'" style="background-image:url(assets/img/flags/'.strtoupper($aVal['langCode']).'.png)">'.
''.$aVal['langName'].'</a>';
			}
		} else {
			$sReturn = '';
		}
		$sReturn = $sReturn;
		return $sReturn;
	}
	
	/**
	 * Create modules navigation
	 * @return	html string
	 */
	public function moduleNavigation() {
		//TODO2: better oop
		$oUser = new \admin\user; 
		$aModules = $oUser->getUserModules();
		$sReturn = '';
		if (count($aModules)==1) return $sReturn;
		$aIcons = array('template'=>'file-image-o', 'site'=>'sitemap', 'content'=>'font');
		foreach ($aModules as $aVal) {
			$sIcon = $aIcons[$aVal['module']];
			$sReturn.= 
'<a href="'.\admin\cms::adminHref($aVal['startpage'], $aVal['module']).'" '.
'class="button '.$aVal['module'].(MODULE==$aVal['module']?' selected':'').'"><i class="fa fa-'.$sIcon.'"></i> '.$aVal['name'].'</a>';
		}
		return $sReturn;
	}
	
	//TODO3: breadcrumb navigation
	public function breadcrumb() {
	}
	
	
	/**
	 * Create static href for anchors
	 * @return	html string
	 */
	public static function adminHref($sPage=PAGE, $sModule=MODULE, $sLangCode=LANG) {
		return '?lang='.$sLangCode.'&module='.$sModule.'&page='.$sPage;
	}
		
	/**
	 * Create Callouts
	 * @return	html string
	 */
	public function successCallout ($sText) {
		return 
'<div class="success callout" data-closable>
	<p>'.$sText.'</p>
	<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
	<span aria-hidden="true">&times;</span>
	</button>
</div>';
	}
	public function alertCallout ($sText) {
		return 
'<div class="success callout" data-closable>
	<p>'.$sText.'</p>
	<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
	<span aria-hidden="true">&times;</span>
	</button>
</div>';
	}
}
