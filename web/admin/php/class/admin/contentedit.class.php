<?php
namespace admin;

class contentedit  {
	protected $DB = false;
	public function __construct() {
		$this->DB = \app\db::load();
	}

	public function getModuleRow ($iModule, $sModuleName, $sFieldName, $sFieldContent) {
		if ($iModule == 12) { //spacer
			 return $this->inputModuleSpacer($sModuleName);
		} else {
			$sRow = '
<tr>
	<td><label for="'.$sFieldName.'" class="middle">'.$sModuleName.'</label>	</td>
	<td>'.$this->getModuleInput ($iModule, $sFieldName, $sFieldContent).'</td>
</tr>';
			return $sRow;
		}
	}
	//TODO: handle/split modules as plugins
	private function getModuleInput ($iModule, $sFieldName, $sFieldContent) {
		switch ($iModule) {
			case 1:
				return $this->inputModuleChar($sFieldName, $sFieldContent);
				break;
			case 2:
				return $this->inputModuleText($sFieldName, $sFieldContent);
				break;
			case 3:
				return $this->inputModuleInt($sFieldName, $sFieldContent);
				break;
			case 4:
				return $this->inputModuleImage($sFieldName, $sFieldContent);
				break;
			case 5:
				return $this->inputModuleFile($sFieldName, $sFieldContent);
				break;
			case 6:
				return $this->inputModuleDate($sFieldName, $sFieldContent);
				break;
			case 7:
				return $this->inputModuleBoolean($sFieldName, $sFieldContent);
				break;
			case 8:
				return $this->inputModuleOptions($sFieldName, $sFieldContent);
				break;
			case 9:
				return $this->inputModulePassword($sFieldName, $sFieldContent);
				break;
			case 11:
				return $this->inputModuleSite($sFieldName, $sFieldContent);
				break;
			case 13:
				return $this->inputModuleImageManger($sFieldName, $sFieldContent);
				break;
			case 14:
				return $this->inputModuleFileManger($sFieldName, $sFieldContent);
				break;
		}
	}

	/**
	 * CONTENT FUNCTION ***********************************************************
	 */
	 
	private function inputModuleSpacer($sModuleName) {
		$sSpacer = '<tr><td colspan="2"><h4>'.$sModuleName.'</h4></td></tr>';
		return $sSpacer;
	}
	
	//
	private function inputModuleChar($sFieldName, $sFieldContent) {
		$sInput = '<input type="text" name="'.$sFieldName.'" id="'.$sFieldName.'" '.
				  'value="'.$sFieldContent.'">';
		return $sInput;
	}
	private function inputModuleText($sFieldName, $sFieldContent) {
		$sInput = '<textarea name="'.$sFieldName.'" id="'.$sFieldName.'">'.
				   $sFieldContent.'</textarea>'.
				   '<script>tinymce.init({
selector:"#'.$sFieldName.'", 
language: "de",
body_class: "tinymce_body",
content_css : "/myLayout.css",
plugins: "link image code",
menubar: false,
toolbar: "styleselect | bold italic | link image",
menu: {
    insert: {title: "Insert", items: "link media | template hr"},
    format: {title: "Format", items: "bold italic superscript subscript | formats | removeformat"},
    table: {title: "Table", items: "inserttable tableprops deletetable | cell row column"}
  }

				   
				   });
				   </script>';
		return $sInput;
	}
	private function inputModuleInt($sFieldName, $sFieldContent) {
		$sInput = '<input type="number" name="'.$sFieldName.'" id="'.$sFieldName.'" '.
				  'value="'.$sFieldContent.'">';
		return $sInput;
	}
	private function inputModuleImage($sFieldName, $sFieldContent) {
		$sInput = '<label for="'.$sFieldName.'" class="button">Bild hochladen</label>
				   <input type="file" id="'.$sFieldName.'" class="show-for-sr" 
				    value="'.$sFieldContent.'" onclick="alert(\'Funktion vorerst nicht verfügbar\');return false;">';
		return $sInput;
	}
	private function inputModuleFile($sFieldName, $sFieldContent) {
		$sInput = '<label for="'.$sFieldName.'" class="button">Datei hochladen</label>
				   <input type="file" id="'.$sFieldName.'" class="show-for-sr" 
				    value="'.$sFieldContent.'" onclick="alert(\'Funktion vorerst nicht verfügbar\');return false;">';
		return $sInput;
	}
	private function inputModuleDate($sFieldName, $sFieldContent) {
		$sInput = '<input type="date" name="'.$sFieldName.'" id="'.$sFieldName.'" '.
				  'value="'.$sFieldContent.'">';
		return $sInput;
	}
	private function inputModuleBoolean($sFieldName, $sFieldContent) {
		$sInput = '<input type="checkbox" name="'.$sFieldName.'" id="'.$sFieldName.'" '.
				  'value="1"'.(!empty($sFieldContent)?' checked':'').'>';
		return $sInput;
	}
	//TODO2:
	private function inputModuleOptions($sFieldName, $sFieldContent) {
		$sInput = '<input type="text" name="'.$sFieldName.'" id="'.$sFieldName.'" '.
				  'value="'.$sFieldContent.'">';
		return $sInput;
	}
	private function inputModulePassword($sFieldName, $sFieldContent) {
		$sInput = '<input type="password" name="'.$sFieldName.'" id="'.$sFieldName.'" '.
				  'value="'.$sFieldContent.'">';
		return $sInput;
	}
	//TODO2:
	private function inputModuleSite($sFieldName, $sFieldContent) {
		$sInput = '<input type="text" name="'.$sFieldName.'" id="'.$sFieldName.'" '.
				  'value="'.$sFieldContent.'">';
		return $sInput;
	}
	private function inputModuleImageManger($sFieldName, $sFieldContent) {
		//TODO2: multiple images/files width individual file names
		$sInput = '<div class="input-group">'.
				  '<span class="input-group-label">'.
				  '<a href="#" onclick="window.open(\'filemanager/dialog.php?type=1&field_id='.$sFieldName.'&relative_url=1&popup=1\', \'fileManagerWindow\', \'width=960,height=600\');return false;">Dateimanager öffnen</a>'.
				  '</span>'.
				  '<input type="text" name="'.$sFieldName.'" id="'.$sFieldName.'" class="input-group-field" value="'.$sFieldContent.'">'.
				  '</div>';
		return $sInput;
	}
	private function inputModuleFileManger($sFieldName, $sFieldContent) {
		//TODO2: multiple images/files width individual file names
		$sInput = '<div class="input-group">'.
				  '<span class="input-group-label">'.
				  '<a href="#" onclick="window.open(\'filemanager/dialog.php?type=2&field_id='.$sFieldName.'&relative_url=1&popup=1\', \'fileManagerWindow\', \'width=960,height=600\');return false;">Dateimanager öffnen</a>'.
				  '</span>'.
				  '<input type="text" name="'.$sFieldName.'" id="'.$sFieldName.'" class="input-group-field" value="'.$sFieldContent.'">'.
				  '</div>';
		return $sInput;
	}

}
