<?php
namespace admin;

class securesession  {
	public function __construct() {
		$this->sec_session_start();
	}

	/**
	 * Make a secure session
	 *
	 * @access	private
	 * @return	session
	 */
	private function sec_session_start() {
		$session_name = 'sec_session_id';
		#avoid javascript connect to the session
		$secure = false;
		#sessions only with cookie
		$httponly = true;
		if (ini_set('session.use_only_cookies', 1) === FALSE) {
		    header('Location: '.__DIR__.'?err=Could not initiate a safe session');
		    exit();
		}
		#get cookie parameters
		$cookieParams = session_get_cookie_params();
		session_set_cookie_params($cookieParams['lifetime'],
		    $cookieParams['path'], 
		    $cookieParams['domain'], 
		    $secure,
		    $httponly);
		#set session name and start session
		session_name($session_name);
		session_start();
		session_regenerate_id();
	}
}