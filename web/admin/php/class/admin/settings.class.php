<?php
namespace admin;

 class settings {
	public function __construct($aConf){
		$oConstants = new \admin\constants($aConf);
		$oConstants = new \admin\statics($aConf);
	}
	public function __clone(){}
 }
 
 
class constants  {
	private $aConf;
	protected $DB = false;
	public function __construct() {
		$this->DB		  = \app\db::load();
		$this->USER		  = new \admin\user(); //TODO2: better oop
		$this->checkLogin = $this->USER->checkLogin();
		$this->defineModule();
		$this->definePage();
		$this->defineNavid();
		$this->defineItemid();
	}	
	public function defineModule() {
		if (!$this->checkLogin) {
			$sModule = 'login';
		} else if (isset($_GET['module'])) {
			$sModule = preg_replace('/[^a-zA-Z0-9_\-]+/', '', $_GET['module']);
		} else {
			$sModule = $this->USER->getUserStartModule();
		}
		define('MODULE', $sModule);
	}
	
	public function definePage() {
		if (!$this->checkLogin) {
			$sPage = 'login';
		} else if (isset($_GET['page'])) {
			$sPage = preg_replace('/[^a-zA-Z0-9_\-]+/', '', $_GET['page']);
		} else {
			$sPage = $this->aConf['modules'][MODULE]['startpage'];
		}
		define('PAGE', $sPage);
	}
	public function defineNavid() {
		if (isset($_GET['navid'])) {
			$iNavid = preg_replace('/[^0-9]+/', '', $_GET['navid']);
		} else {
			$iNavid = 0;
		}
		define('NAVID', $iNavid);
	}
	public function defineItemid() {
		if (isset($_GET['itemid'])) {
			$iItemid = preg_replace('/[^0-9]+/', '', $_GET['itemid']);
		} else {
			$iItemid = 0;
		}
		define('ITEMID', $iItemid);
	}
}

 
class statics {
	static public $aConf = null;
	public function __construct($aConf) {
		self::$aConf = $aConf;
	}
}