<?php
//GENERAL TODO2:
//check input variables
//GENERAL TODO3:
//all used classes load in one

//set base directory of admin
$s_admin_basedir = dirname(__FILE__).DIRECTORY_SEPARATOR;
define ('ADMIN_BASEDIR', $s_admin_basedir);

$enc = getenv('HTTP_ACCEPT_ENCODING');
if (strstr($enc, 'gzip')) {
	ob_start('ob_gzhandler');
} else {
	ob_start();
}

//load php helpers
require '../php/config/config.php';
require '../php/class/autoload.php';

//classes
$oSecuresession = new \admin\securesession;
$oDb			= new \app\db($host, $username, $password, $database);
$oSettings		= new \admin\settings($a_conf);
$oLang			= new \app\lang;

$oUser			= new \admin\user();
$oCms			= new \admin\cms();
?>

<!doctype html>
<html class="no-js" lang="de">
<head>
	<meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>red.eins</title>
	<link rel="stylesheet" href="css/app.css">
	<link rel="stylesheet" href="bower_components/jstree/dist/themes/default/style.min.css">
	<link rel="stylesheet" href="bower_components/DataTables/datatables.min.css">
</head>
<body>

<?php
if ($oUser->checkLogin()) {
	require_once('html/partials/header.php');
	require_once('html/partials/breadcrumb.php');
}
?>
    
<div class="row">
	<div class="large-12 columns">
			
<?php
	//include page files
	$oCms->loadPages(MODULE, PAGE);
?>
	</div>
</div>

<script src="bower_components/jquery/dist/jquery.js"></script>
<script src="bower_components/what-input/what-input.js"></script>
<script src="bower_components/foundation-sites/dist/foundation.js"></script>
<script src="bower_components/TableDnD/dist/jquery.tablednd.js"></script>
<script src="bower_components/jstree/dist/jstree.min.js"></script>
<script src="bower_components/DataTables/datatables.min.js"></script>
<script src="js/min/app-min.js"></script>
</body>
</html>

<?php
ob_end_flush();
?>