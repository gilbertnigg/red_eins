<header>
	<div class="row-top">
		<div class="row">
			<div class="large-12 columns">
				<div class="float-left">
					<a href="<?=$oUser->homeUrl()?>" class="small button"><i class="fa fa-home"></i> Home</a>
				</div>
				<div class="float-right small button-group">
					<a href="<?=$oCms->adminHref('user','user')?>" class="button"><i class="fa fa-user"></i> Benutzerkonto</a>
					<a href="?logout" class="button"><i class="fa fa-sign-out"></i> Abmelden</a>
<!--
TODO: as dropdown
<button class="button float-right" type="button" data-toggle="user-dropdown">[Name]</button></p>
<div class="dropdown-pane bottom" id="user-dropdown" data-dropdown>
<a href="<?=$oCms->adminHref('user','user')?>" class="button"><i class="fa fa-user"></i> Benutzerkonto</a><br>
<a href="?logout" class="button"><i class="fa fa-sign-out"></i> Abmelden</a>
</div>
-->
				</div>
			</div>
		</div>
	</div>
	<div class="row-bottom">
		<div class="row">
			<div class="large-12 columns button-group">
				<div class="float-left"><?=$oCms->moduleNavigation()?></div>
				<div class="float-right"><?=$oCms->languageNavigation()?></div>
			</div>
		</div>
	</div>
</header>