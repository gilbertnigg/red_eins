$(document).foundation();


$('#dndTableTemplateModules').tableDnD({
        onDragClass: "drag",
        onDrop: function(table, row) {
            var rows = table.tBodies[0].rows;
            var sNewModuleOrder = "";
            for (var i=0; i<rows.length; i++) {
                sNewModuleOrder+= (sNewModuleOrder?",":"")+rows[i].id;
            }
			$('#dndtable tr').removeClass("oktr");
			$('#'+row.id).addClass("oktr");
			var iTemplateId = $('#dndTableTemplateModules').data('templateId');
			console.log("templateid="+iTemplateId+"&moduleorder="+sNewModuleOrder+"&action=dnd_sort");
           $.ajax({
				type: "POST",
				url: "php/partials/template_module_dnd.php",
				data: "templateid="+iTemplateId+"&moduleorder="+sNewModuleOrder+"&action=dnd_sort",
				success: function(msg){
					console.log(">"+msg);
				}
			});
        },
        dragHandle: ".drag-handle"
    })


//$(document).ready(function() {
	$(function () {
		$('#jstreeContent').jstree({
			"types" : {
				"default" : {
					"icon" : "fa fa-folder-o"
				}
			},
			"plugins" : [ "types", "state" ]
		});
	});
	
	$(function () {
		$('#jstreeSite').jstree({
			"types" : {
				"default" : {
					"icon" : "fa fa-bars"
				}
			},
			"core" : {
				"check_callback" : true, 
				"themes" : {
					"variant" : "large", 
					"dots": true,
					"stripes": false,
					"responsive": true
				}, 
				"multiple": false, 
				"animation": 200, 
				"expand_selected_onload": true

			},
			"plugins" : [ "types", "state", "dnd" ]
		}).bind("move_node.jstree", function (e, data, node, parent, position) {
			var iNavId	  = data.node.id;
			var iParentId = (data.node.parent=='#' ? 0 : data.node.parent);
			var iPosition = data.position;
			var oNode	  = $('#'+iNavId);
			var sLangCode = $('#jstreeSite').data('langcode');
			//console.log("navid="+iNavId+"&parent="+iParentId+"&langcode="+sLangCode+"&order="+iPosition+"&action=dnd_move");
			$.ajax({
				type: "POST",
				url: "php/partials/site_dnd.php",
				data: "navid="+iNavId+"&parent="+iParentId+"&langcode="+sLangCode+"&order="+iPosition+"&action=dnd_move",
				success: function(msg) {
					console.log(">"+msg);
				}
			});
	   	});
	});
	
	
	//site
	function create_site(sUrl) {
		window.location.href = sUrl;
		return false;
	}
	function update_site(sUrl) {
		window.location.href = sUrl;
		return false;
	}
	function delete_site(sUrl) {
		if (confirm('Seite dauerhaft löschen?')) {
			window.location.href = sUrl;
		}
		return false;
	}
	//content
	function update_content(sUrl) {
		window.location.href = sUrl;
		return false;
	}
	/**/
$(document).ready(function() {
	//gui
	$('.success.button').click(function() {
		$(this).find('.fa').removeClass('fa-dot-circle-o').addClass('fa-spinner fa-spin');
	});
	
	//TODO: datatables
	/*$('#contentsTable').dataTable( {
		"columnDefs": [ {
			"targets": 'no-sort',
			"orderable": false,
		}]
	});*/
});
	
    //open foundation tabs
	var link_tab = window.location.hash.substr(1);
	if (link_tab) {
		try {
			$('[data-tabs]').eq(0).foundation('selectTab', $('#'+link_tab));
		} catch(err) {
		}
	}
//});