# README #

## Redaktionssystem red.eins

red.eins ist ein Redaktionssystem basierend auf PHP. Es wird von [Gilbert Nigg][tech_gilles] als Diplomarbeit für den Lehrgang WebProgrammer PHP 25 der [EB Zürich][ebzuerich] entwickelt. 

### Version
1.0

### Demo
Demo Version unter: 
http://test.gilles.ch/red_eins/web/admin/

### Dokumentation
Dokumentation als PDF-Datei unter: 
http://test.gilles.ch/red_eins/konzept/pdf/Konzept_Redaktionssystem_red_eins.pdf

### SQL Installation
SQL Dump unter: 
http://test.gilles.ch/red_eins/web/-sqldump.sql

### Tech

Für die Installation wird folgende Software benötigt
  - Apache-Server
  - PHP >= 5.6
  - MySql >= 5.5

red.eins ist ein Open Source Projekt mit einem [public repository][red_eins_git] auf GitHub.

### Todos

 - Readme ergänzen
 - Fertigstellung Codes
 - npm Installation
 - Templates mit http://twig.sensiolabs.org/

### License

MIT


**Free Software, Hell Yeah!**

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [Demo Version]: <http://test.gilles.ch/red_eins/web/admin>
   [red_eins_git]: <https://bitbucket.org/gilbertnigg/red_eins>
   [red_eins_git_repo]: <https://gilbertnigg@bitbucket.org/gilbertnigg/red_eins.git>
   [tech_gilles]: <mailto:tech@gilles.ch>
   [gilbert nigg]: <https://gilles.ch>
   [ebzuerich]: <http://www.eb-zuerich.ch/>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>